<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Sample.aspx.cs" Inherits="Sample" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <body class="page-header-fixed compact-menu page-horizontal-bar">
 <div id="main-wrapper" class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Basic example</h4>
                                </div>
                                <div class="panel-body">
                                   <div class="table-responsive">
                                   
                                   <%
                                     System.Data.DataTable dt = new System.Data.DataTable();
                                     dt = GetData();
                                     string LocCode = "";
                                     string UserName = "";
                                     string UserType = "";
                                     string Sno = "";
                                    if (dt.Rows.Count > 0)
                                    {
 
                                    %>
                                   
                                   
                                   
                                    <button id="button">Delete selected row</button>
                                    <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
                                        <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>SNo</th>
                                                    <th>Location Name</th>
                                                    <th>User Name</th>
                                                    <th>User Type</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                                                </tr>
                                            </thead>
                                        <tfoot>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>SNo</th>
                                                    <th>Location Name</th>
                                                    <th>User Name</th>
                                                    <th>User Type</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                                                </tr>
                                        </tfoot>
                                        <tbody>
                                        
                                        <%
                                        foreach (System.Data.DataRow dr in dt.Rows)
                                        {
                                            Sno = dr["SNo"].ToString();
                                            LocCode = dr["LocCode"].ToString();
                                            UserName = dr["UserName"].ToString();
                                            UserType = dr["UserType"].ToString();

                                            
                                            
                                        %>
                                        <tr>
                                                                               
                                           <%-- <asp:CheckBoxList ID="CheckBoxList1" runat="server">
                                            </asp:CheckBoxList>--%>
                                            <%--<td><asp:CheckBox ID="CheckBox1" runat="server" /></td>--%>
                                             <td><asp:CheckBox ID="CheckBox1" runat="server" /></td>
                                             <td><%=LocCode%></td>
                                             <td><%=UserName%></td>
                                             <td><%=UserType%></td>
                                              <td><%=Sno%></td>
                                              <td>&#39;<a href="" class="editor_edit">Edit</a> / <a href="" class="editor_remove">
                                                  Delete</a></td>
                                              
                                              
                                             <td><p data-placement="top" data-toggle="tooltip" title="Edit">
                                               <span class="glyphicon glyphicon-pencil"><asp:Button ID="BtnEdit" runat="server" class="btn btn-primary btn-xs" 
                                                     data-title="Edit" data-toggle="modal" data-target="#edit" onclick="BtnEdit_Click"/></span></p></td>
                                             <td><p data-placement="top" data-toggle="tooltip" title="Delete">
                                                 <asp:Button ID="BtnDelete" runat="server" class="btn btn-danger btn-xs" 
                                                     data-title="Delete" data-toggle="modal" data-target="#delete" 
                                                     onclick="BtnDelete_Click" /><span class="glyphicon glyphicon-trash"></span></p></td>
                                            
                                             <td><button class="btn btn-danger btn-xs" id="Button1" data-toggle="modal" data-target="#myModal" data-id="@item.ID"> 
                                                 Delete </button><td>
                                             <%--<td><p data-placement="top" data-toggle="tooltip" title="Edit"><button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit" ><span class="glyphicon glyphicon-pencil"></span></button></p></td>
                                             <td><p data-placement="top" data-toggle="tooltip" title="Delete"><button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p></td>--%>
                                            
                                             
                                        </tr>
                                       <%
                                           if (CheckBox1.Checked == true)
                                           {
                                               dr.Delete();
                                               dr.AcceptChanges();

                                               //for (int i = 0; i < dr.Columns.Count; i++)
                                               //{
                                               //    if (CheckBox1.Checked == true)
                                               //    {
                                               //        dr.Rows[i - 1].Delete();
                                               //        dr.AcceptChanges();
                                               //    }
                                               //}

                                           }
                                                                                    
                                        
                                        } 
                                       %>
                                      </tbody>
                                   </table>
                                  
                                    <%
                                    } 
                                    %>
                                        
                                        
                                        
                                           
                                            
                                           
                                           
                                           
                                            
                                         
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div><!-- Row -->
                </div>
        <!-- Javascripts -->
        <script src="assets/plugins/jquery/jquery-2.1.3.min.js"></script>
        <script src="assets/plugins/jquery-ui/jquery-ui.min.js"></script>
        <script src="assets/plugins/pace-master/pace.min.js"></script>
        <script src="assets/plugins/jquery-blockui/jquery.blockui.js"></script>
        <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <script src="assets/plugins/switchery/switchery.min.js"></script>
        <script src="assets/plugins/uniform/jquery.uniform.min.js"></script>
        <script src="assets/plugins/classie/classie.js"></script>
        <script src="assets/plugins/waves/waves.min.js"></script>
        <script src="assets/plugins/3d-bold-navigation/js/main.js"></script>
        <script src="assets/plugins/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
        <script src="assets/plugins/jquery-validation/jquery.validate.min.js"></script>
        <script src="assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="assets/js/modern.min.js"></script>
        <script src="assets/js/pages/form-wizard.js"></script>
        
    </body>
</asp:Content>

