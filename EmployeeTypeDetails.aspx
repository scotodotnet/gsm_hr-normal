﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="EmployeeTypeDetails.aspx.cs" Inherits="EmployeeTypeDetails" Title="Untitled Page" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

    <script>
        $(document).ready(function () {
            $('#tableEmpType').dataTable();
        });
    </script>


 <script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#tableEmpType').dataTable();               
            }
        });
    };
    </script>
    

                           <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate> 
                                    
                                    
                 <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">Employee Type Details</li></h4> 
                    </ol>
                </div>
<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
              
            <div class="col-md-9">
			<div class="panel panel-white">
			    <div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Employee Type</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
           
                        <form class="form-horizontal">


<div class="panel-body">
				                          			                          
					                           <div class="form-group row">
																	
						<asp:Button ID="btnEmpType" class="btn btn-primary btn-rounded" runat="server" Text="ADD"/>
						
						  
						
						
						 
<cc1:ModalPopupExtender ID="MPE2" runat="server" PopupControlID="Panel2" TargetControlID="btnEmpType"
CancelControlID="" BackgroundCssClass="Background">
</cc1:ModalPopupExtender>
<asp:Panel ID="Panel2" runat="server" CssClass="Popup" align="center" BackColor="White" Height="269px" Width="400px" style="display:none">
				                          	<table width="100%" style="border:Solid 3px #EEE8AA; width:100%; height:100%" cellpadding="0" cellspacing="0">
<tr style="background-color:#D3D3D3">
<td colspan="2" style=" height:10%; color:White; font-weight:bold; font-size:larger" align="center">Employee Type</td>
</tr>
<tr>
<td align="right">
Employee Type
</td>
<td>
<asp:TextBox ID="txtEmployeeType" class="form-control" runat="server" required></asp:TextBox>
</td>
</tr>
<tr>
<td>
</td>
<td>				                           <!-- Button start -->
                               
                            
                           <asp:LinkButton ID="btnSaveEmployeeType" runat="server" class="btn btn-success" onclick="btnSaveEmployeeType_Click" >Save</asp:LinkButton>
                           <asp:LinkButton ID="BtnClearEmployeeType" runat="server" class="btn btn-danger" onclick="BtnClearEmployeeType_Click"  >Close</asp:LinkButton>  
                            
                    </td>
</tr>
</table>						
</asp:Panel>
</div>
                                                                              
		                                       <div class="col-md-12">
		                                      <div class="row">
                      <asp:Repeater ID="rptrEmplyeeType" runat="server" onitemcommand="rptrEmplyeeType_ItemCommand">
                    <HeaderTemplate>
                       <table id="tableEmpType" class="display table" style="width: 100%; cellspacing: 0;">
                    <thead>
                        <tr>
                                                   
                                                    <th>TypeName</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    
                  
                    
                  
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "TypeName")%>
                    </td>
                  
                    <td>
                       <asp:LinkButton ID="lnkedit" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("TypeID") %>' CommandName="Edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p></td>
                      </td>
                     <td>
                       <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger btn-xs"  CommandArgument='<%#Eval("TypeID") %>' CommandName="Delete"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                    </td>
                    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
              
                </table>
            </FooterTemplate>
        </asp:Repeater>
                            </div>
                        </div>
                       
                                   
						           </div>
	
	    </form>
					 </div>	                        
						                
					</form>
						            	</div><!-- panel white end -->
		   
		    
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
                       <!-- Dashboard start -->
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>  
                        
                          </div>
                        <!-- Dashboard End -->   
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
      </div>        	                 
						            </ContentTemplate>
                             </asp:UpdatePanel>   


</asp:Content>

