﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;

public partial class EmpWiseOT : System.Web.UI.Page
{
    DataTable AutoDataTable = new DataTable();
    DataTable PunchTable = new DataTable();
    string ModeType;
    string Date1;
   // string Date2;
    string SSQL;
    DataTable dsEmployee = new DataTable();
    DataTable DataCells = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    string mIpAddress_IN;
    string mIpAddress_OUT;
    string Machine_ID_Str;
    string Date_Value_Str;
    string Date_Value_Str1;
    int shiftCount = 0;
    DataTable mLocalDS = new DataTable();
    DataTable mEmployeeDS = new DataTable();
    DateTime fromdate;
    DateTime todate;
    string SessionUserType;
    string halfPresent;

    double OT_Time = 0;
    string Wages;


    DateTime date1;
    DateTime date2 = new DateTime();
    //string Date_Value_Str1;
    DateTime InTime_Check;
    DateTime InTime_Check1;
    DateTime InToTime_Check;
    TimeSpan InTime_TimeSpan;
    string From_Time_Str;

    TimeSpan ts4 = new TimeSpan();


    System.Web.UI.WebControls.DataGrid GridView1 =
                 new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Day Attendance Summary";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["UserType"].ToString();

            //ModeType = Request.QueryString["ModeType"].ToString();
            Date1 = Request.QueryString["Date1"].ToString();
            //Date2 = Request.QueryString["Date2"].ToString();

            DataTable dtIPaddress = new DataTable();

            dtIPaddress = objdata.IPAddressForAll(SessionCcode.ToString(), SessionLcode.ToString());
            if (dtIPaddress.Rows.Count > 0)
            {
                for (int i = 0; i < dtIPaddress.Rows.Count; i++)
                {
                    if (dtIPaddress.Rows[i]["IPMode"].ToString() == "IN")
                    {
                        mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
                    }
                    else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "OUT")
                    {
                        mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
                    }
                }
            }

            Fill_Multi_timeIN();
            Write_MultiIN();



            GridView1.HeaderStyle.Font.Bold = true;
            GridView1.DataSource = DataCells;
            GridView1.DataBind();
            string attachment = "attachment;filename=OT.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";

            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            GridView1.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td font-Bold='true' colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\">" + SessionCompanyName + "</a>");
            Response.Write("--");
            Response.Write("<a style=\"font-weight:bold\">" + SessionLocationName + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td font-Bold='true' colspan='12'>");
            Response.Write("<a style=\"font-weight:bold\"> EMPLOYEE WISE OT REPORT &nbsp;&nbsp;&nbsp;" + Date1 + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            //Response.Write("<tr Font-Bold='true' align='center'>");
            //Response.Write("<td font-Bold='true' colspan='8'>");
            //Response.Write("<a style=\"font-weight:bold\">" + Date + "</a>");
            //Response.Write("</td>");
            //Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();






            //UploadDataTableToExcel(DataCells);

        }
    }
    public void Fill_Multi_timeIN()
    {
        try
        {
            //DateTime fromdate = Convert.ToDateTime(Date1);
            //DateTime todate = Convert.ToDateTime(Date2);
            //int dayCount = (int)((todate - fromdate).TotalDays);
            //if (dayCount > 0)
            //{
            //AutoDataTable.Columns.Add("DAY ATTENDANCE SUMMARY");
            //AutoDataTable.NewRow();
            //AutoDataTable.Rows.Add();


            //AutoDataTable.NewRow();
            //AutoDataTable.Rows.Add("DAY ATTENDANCE SUMMARY");
            //AutoDataTable.NewRow();
            //AutoDataTable.Rows.Add();
            //AutoDataTable.Columns.Add("SNo");
            AutoDataTable.Columns.Add("DeptName");
            AutoDataTable.Columns.Add("MachineID");
            AutoDataTable.Columns.Add("MachineID_Encrypt");
            AutoDataTable.Columns.Add("EmpNo");
            AutoDataTable.Columns.Add("ExistingCode");
            AutoDataTable.Columns.Add("FirstName");
            AutoDataTable.Columns.Add("TimeIN1");
            AutoDataTable.Columns.Add("TimeOUT1");
            AutoDataTable.Columns.Add("TimeIN2");
            AutoDataTable.Columns.Add("TimeOUT2");


            SSQL = "";
            SSQL = "select isnull(DeptName,'') as [DeptName], Cast(MachineID As int) As MachineID,MachineID_Encrypt";
            SSQL = SSQL + ",EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
            SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
            SSQL = SSQL + " from Employee_Mst Where Compcode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IsActive='Yes' and OTEligible='Yes'";
            if (SessionUserType == "2")
            {
                SSQL = SSQL + " And IsNonAdmin='1'";
            }
            SSQL = SSQL + " Order By MachineID";

            dsEmployee = objdata.ReturnMultipleValue(SSQL);

            if (dsEmployee.Rows.Count <= 0)
            {
                return;
            }
            else
            {
                Int32 DSVAL = 0;
                for (int i = 0; i < dsEmployee.Rows.Count; i++)
                {
                    AutoDataTable.NewRow();
                    AutoDataTable.Rows.Add();

                    AutoDataTable.Rows[DSVAL][0] = dsEmployee.Rows[i]["DeptName"].ToString();
                    AutoDataTable.Rows[DSVAL][1] = dsEmployee.Rows[i]["MachineID"].ToString();
                    AutoDataTable.Rows[DSVAL][2] = dsEmployee.Rows[i]["MachineID_Encrypt"].ToString();
                    AutoDataTable.Rows[DSVAL][3] = dsEmployee.Rows[i]["EmpNo"].ToString();
                    AutoDataTable.Rows[DSVAL][4] = dsEmployee.Rows[i]["ExistingCode"].ToString();
                    AutoDataTable.Rows[DSVAL][5] = dsEmployee.Rows[i]["FirstName"].ToString();

                    DSVAL += 1;
                }
            }
            //}
        }
        catch (Exception e)
        {
        }
    }

    public void Write_MultiIN()
    {
        try
        {
            int intI = 1;
            int intK = 1;
            int intCol = 0;

            string[] Time_Minus_Value_Check;


            DataCells.Columns.Add("SNo");
            DataCells.Columns.Add("DeptName");
            // DataCells.Columns.Add("MachineID");
            DataCells.Columns.Add("EmpNo");
            DataCells.Columns.Add("ExistingCode");
            DataCells.Columns.Add("FirstName");
            DataCells.Columns.Add("TimeIN");
            DataCells.Columns.Add("TimeOUT");
            DataCells.Columns.Add("WorkHours");
            DataCells.Columns.Add("OT");

            for (intCol = 0; intCol < AutoDataTable.Rows.Count; intCol++)
            {
                DataCells.NewRow();
                DataCells.Rows.Add();

                //DataCells.Rows[intCol]["SNo"] = intCol + 1;
                //DataCells.Rows[intCol]["DeptName"] = AutoDataTable.Rows[intCol]["DeptName"];
                //// DataCells.Rows[intCol]["MachineID"] = AutoDataTable.Rows[intCol]["MachineID"];
                //DataCells.Rows[intCol]["EmpNo"] = AutoDataTable.Rows[intCol]["EmpNo"];
                //DataCells.Rows[intCol]["ExistingCode"] = AutoDataTable.Rows[intCol]["ExistingCode"];
                //DataCells.Rows[intCol]["FirstName"] = AutoDataTable.Rows[intCol]["FirstName"];
            }




            fromdate = Convert.ToDateTime(Date1);
            //todate = Convert.ToDateTime(Date2);
            //int dayCount = (int)((todate - fromdate).TotalDays);

            //int daysAdded = 0;

            intK = 1;
            intI = 1;
            intCol = 0;

            for (int intRow = 0; intRow < AutoDataTable.Rows.Count; intRow++)
            {

                //Punch Varilable

                string FirstIN = "";
                string FirstOUT = "";
                string SecondIN = "";
                string SecodOUT = "";



                int colIndex = intK;
                bool isPresent = false;
                isPresent = false;
                string Machine_ID_Str = "";
                string OT_Week_OFF_Machine_No = "";
                string Date_Value_Str = "";
                string Date_Value_Str1 = "";

                string Time_IN_Str = "";
                string Time_Out_Str = "";
                string Total_Time_get = "";
                Int32 j = 0;
                double time_Check_dbl = 0;
                isPresent = false;



                Machine_ID_Str = AutoDataTable.Rows[intRow][2].ToString();
                Date_Value_Str = string.Format(Date1, "dd-MM-yyyy");

                DateTime Dateval = Convert.ToDateTime(Date_Value_Str).AddDays(1);

                Date_Value_Str1 = string.Format(Convert.ToString(Dateval), "dd-MM-yyyy");

                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
                SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                SSQL = SSQL + " And TimeIN >='" + fromdate.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";

                mLocalDS = objdata.ReturnMultipleValue(SSQL);
                if (mLocalDS.Rows.Count >= 1)
                {
                    for (int ival = 0; ival < mLocalDS.Rows.Count; ival++)
                    {
                        if (ival <= 0)
                        {
                            int ival1 = 6 + ival;

                            AutoDataTable.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS.Rows[ival]["TimeIN"]);
                            FirstIN = string.Format("{0:hh:mm tt}", mLocalDS.Rows[ival]["TimeIN"]);
                        }

                    }
                }
                DataTable mLocalDS1 = new DataTable();

                string Emp_Total_Work_Time_1 = "00:00";
                //Double time_Check_dbl = 0;

                string From_Time_Str = "";
                string To_Time_Str = "";
                string Final_InTime = "";
                string Final_OutTime = "";
                string Final_Shift = "";
                DataTable Shift_DS = new DataTable();
                Int32 k = 0;
                bool Shift_Check_blb = false;
                string Shift_Start_Time = null;
                string Shift_End_Time = null;
                string Employee_Time = "";
                DateTime ShiftdateStartIN = default(DateTime);
                DateTime ShiftdateEndIN = default(DateTime);
                DateTime EmpdateIN = default(DateTime);

                if (mLocalDS.Rows.Count != 0)
                {
                    SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc like '%SHIFT%'";

                    Shift_DS = objdata.ReturnMultipleValue(SSQL);
                    Shift_Check_blb = false;
                    Final_InTime = mLocalDS.Rows[0]["TimeIN"].ToString();
                    for (k = 0; k < Shift_DS.Rows.Count; k++)
                    {

                        string a = Shift_DS.Rows[k]["StartIN_Days"].ToString();
                        int b = Convert.ToInt16(a.ToString());
                        Shift_Start_Time = fromdate.AddDays(b).ToString("dd/MM/yyyy") + " " + Shift_DS.Rows[k]["StartIN"].ToString();
                        string a1 = Shift_DS.Rows[k]["EndIN_Days"].ToString();
                        int b1 = Convert.ToInt16(a1.ToString());
                        Shift_End_Time = fromdate.AddDays(b1).ToString("dd/MM/yyyy") + " " + Shift_DS.Rows[k]["EndIN"].ToString();

                        ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time.ToString());
                        ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time.ToString());

                        EmpdateIN = Convert.ToDateTime(Final_InTime.ToString());

                        if (EmpdateIN >= ShiftdateStartIN & EmpdateIN <= ShiftdateEndIN)
                        {
                            Final_Shift = Shift_DS.Rows[k]["ShiftDesc"].ToString();
                            Shift_Check_blb = true;
                            break;
                        }
                    }
                    if (Shift_Check_blb == false)
                    {
                        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                        SSQL = SSQL + " And TimeOUT >='" + fromdate.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                    }
                    else
                    {
                        if (Final_Shift == "SHIFT1")
                        {
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeOUT >='" + fromdate.AddDays(0).ToString("yyyy/MM/dd") + " " + "06:30' And TimeOUT <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";
                        }
                        else
                        {
                            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                            SSQL = SSQL + " And TimeOUT >='" + fromdate.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                        }
                    }
                }
                else
                {
                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And TimeOUT >='" + fromdate.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + fromdate.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
                }

                mLocalDS1 = objdata.ReturnMultipleValue(SSQL);


                if (mLocalDS1.Rows.Count >= 1)
                {
                    for (int ival = 0; ival < mLocalDS1.Rows.Count; ival++)
                    {
                        if (ival <= 0)
                        {
                            int ival1 = 7 + ival;
                            AutoDataTable.Rows[intRow][ival1] = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[ival]["TimeOUT"]);
                            FirstOUT = string.Format("{0:hh:mm tt}", mLocalDS1.Rows[ival]["TimeOUT"]);
                        }


                    }
                }

                #region OT Calc

                if (mLocalDS.Rows.Count > 1)
                {
                    for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
                    {
                        Time_IN_Str = mLocalDS.Rows[tin][0].ToString();

                        if (mLocalDS1.Rows.Count > tin)
                        {
                            Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
                        }
                        else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
                        {
                            Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
                        }
                        else
                        {
                            Time_Out_Str = "";
                        }


                        ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                        if (mLocalDS.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
                        }
                        if (Time_IN_Str == "" || Time_Out_Str == "")
                        {
                            time_Check_dbl = time_Check_dbl;
                        }
                        else
                        {

                            date1 = System.Convert.ToDateTime(Time_IN_Str);
                            date2 = System.Convert.ToDateTime(Time_Out_Str);

                            TimeSpan ts = new TimeSpan();
                            ts = date2.Subtract(date1);
                            Total_Time_get = Convert.ToString(ts.Hours);
                            ts4 = ts4.Add(ts);
                            Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;



                            string tsfour = Convert.ToString(ts4.Minutes);
                            string tsfour1 = Convert.ToString(ts4.Hours);



                            if (Left_Val(tsfour, 1) == "-" | Left_Val(tsfour1, 1) == "-" | Emp_Total_Work_Time_1 == "0:0")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }

                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts = date2.Subtract(date1);
                                ts = date2.Subtract(date1);
                                Total_Time_get = ts.Hours.ToString();
                                time_Check_dbl = Convert.ToInt16(Total_Time_get.ToString());
                                //time_Check_dbl = Convert.ToInt16(time_Check_dbl) + Convert.ToInt16(Total_Time_get.ToString());
                                Emp_Total_Work_Time_1 = ts.Hours + ":" + ts.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                            }
                            else
                            {
                                time_Check_dbl = Convert.ToInt16(time_Check_dbl) + Convert.ToInt16(Total_Time_get.ToString());
                            }

                        }
                    }
                }
                else
                {
                    TimeSpan ts4 = new TimeSpan();
                    ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Time_IN_Str = "";
                    }
                    else
                    {
                        Time_IN_Str = mLocalDS.Rows[0][0].ToString();
                    }
                    for (int tout = 0; tout <= mLocalDS1.Rows.Count - 1; tout++)
                    {
                        if (mLocalDS1.Rows.Count <= 0)
                        {
                            Time_Out_Str = "";
                        }
                        else
                        {
                            Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
                        }
                    }

                    if (Time_IN_Str == "" || Time_Out_Str == "")
                    {
                        time_Check_dbl = time_Check_dbl;
                    }

                    else
                    {

                        date1 = System.Convert.ToDateTime(Time_IN_Str);
                        date2 = System.Convert.ToDateTime(Time_Out_Str);

                        TimeSpan ts = new TimeSpan();
                        ts = date2.Subtract(date1);
                        Total_Time_get = Convert.ToString(ts.Hours);
                        ts4 = ts4.Add(ts);
                        Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;



                        string tsfour = Convert.ToString(ts4.Minutes);
                        string tsfour1 = Convert.ToString(ts4.Hours);



                        if (Left_Val(tsfour, 1) == "-" | Left_Val(tsfour1, 1) == "-" | Emp_Total_Work_Time_1 == "0:0")
                        {
                            Emp_Total_Work_Time_1 = "00:00";
                        }

                        if (Left_Val(Total_Time_get, 1) == "-")
                        {
                            date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                            ts = date2.Subtract(date1);
                            ts = date2.Subtract(date1);
                            Total_Time_get = ts.Hours.ToString();
                            time_Check_dbl = Convert.ToInt16(Total_Time_get.ToString());
                            //time_Check_dbl = Convert.ToInt16(time_Check_dbl) + Convert.ToInt16(Total_Time_get.ToString());
                            Emp_Total_Work_Time_1 = ts.Hours + ":" + ts.Minutes;
                            Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                            if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }
                            if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                            {
                                Emp_Total_Work_Time_1 = "00:00";
                            }
                        }
                        else
                        {
                            time_Check_dbl = Convert.ToInt16(time_Check_dbl) + Convert.ToInt16(Total_Time_get.ToString());
                        }

                    }
                }

                string Emp_Total_Work_Time;
                string Final_OT_Work_Time;
                string Final_OT_Work_Time_Val;
                //String Emp_Total_Work_Time;
                string Employee_Week_Name;
                string Assign_Week_Name;
                //mLocalDS == null;

                Emp_Total_Work_Time = Emp_Total_Work_Time_1;

                Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString();
                SSQL = "Select WeekOff from Employee_Mst where MachineID='" + OT_Week_OFF_Machine_No + "'";
                mLocalDS = objdata.ReturnMultipleValue(SSQL);
                if (mLocalDS.Rows.Count <= 0)
                {
                    Assign_Week_Name = "";
                }
                else
                {
                    Assign_Week_Name = mLocalDS.Rows[0]["WeekOff"].ToString();
                }
                Boolean NFH_Day_Check = false;
                DateTime NFH_Date;
                DateTime DOJ_Date_Format;
                DateTime Date_Value = new DateTime();

                if (Employee_Week_Name.ToString() == Assign_Week_Name.ToString())
                {
                    //Parthi OT Time Start
                    if (time_Check_dbl >= 9)
                    {
                        isPresent = true;
                        halfPresent = "0";
                        OT_Time = OT_Time + (time_Check_dbl - 1);

                    }
                    else
                    {
                        isPresent = false;
                        halfPresent = "0";
                    }
                    //OT Time End
                }
                else
                {
                    double Calculate_Attd_Work_Time = 0;
                    double Calculate_Attd_Work_Time_half = 0;

                    SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' and Calculate_Work_Hours <> ''";
                    mLocalDS = objdata.ReturnMultipleValue(SSQL);
                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Calculate_Attd_Work_Time = 9;
                    }
                    else
                    {
                        //Calculate_Attd_Work_Time = (!string.IsNullOrEmpty(mLocalDS.Rows[0]["Calculate_Work_Hours"]) ? mLocalDS.Rows[0]["Calculate_Work_Hours"] : 0);
                        if (Calculate_Attd_Work_Time == 0 || Calculate_Attd_Work_Time == null)
                        {
                            Calculate_Attd_Work_Time = 9;
                        }
                        else
                        {
                            Calculate_Attd_Work_Time = 9;
                        }

                    }

                    Calculate_Attd_Work_Time_half = 4.0;
                    halfPresent = "0";

                    //Parthi OT Time Start

                    if (Wages == "STAFF salary")
                    {
                        if (time_Check_dbl >= 10)
                        {
                            isPresent = true;
                            halfPresent = "0";
                            OT_Time = OT_Time + (time_Check_dbl - 9);

                        }
                        else
                        {
                            isPresent = false;
                            halfPresent = "0";
                        }
                    }
                    else
                    {
                        if (time_Check_dbl >= Calculate_Attd_Work_Time)
                        {
                            isPresent = true;
                            halfPresent = "0";
                            OT_Time = OT_Time + (time_Check_dbl - 8);

                        }
                        else
                        {
                            isPresent = false;
                            halfPresent = "0";
                        }
                    }

                    //OT Time End

                }


                #endregion

                if (FirstIN != "" && FirstOUT != "" && Convert.ToDecimal(time_Check_dbl) >= Convert.ToDecimal(9))
                {
                    DataCells.NewRow();
                    DataCells.Rows.Add();

                    DataCells.Rows[intCol]["SNo"] = intCol + 1; 
                    DataCells.Rows[intCol]["DeptName"] = AutoDataTable.Rows[intRow]["DeptName"];
                    // DataCells.Rows[intCol]["MachineID"] = AutoDataTable.Rows[intCol]["MachineID"];
                    DataCells.Rows[intCol]["EmpNo"] = AutoDataTable.Rows[intRow]["EmpNo"];
                    DataCells.Rows[intCol]["ExistingCode"] = AutoDataTable.Rows[intRow]["ExistingCode"];
                    DataCells.Rows[intCol]["FirstName"] = AutoDataTable.Rows[intRow]["FirstName"];

                    DataCells.Rows[intCol]["TimeIN"] = FirstIN;
                    DataCells.Rows[intCol]["TimeOUT"] = FirstOUT;
                    DataCells.Rows[intCol]["WorkHours"] = time_Check_dbl;
                    DataCells.Rows[intCol]["OT"] = OT_Time;

                    intCol += 1;
                }
                else
                {
                   
                }

               

          
                colIndex += shiftCount;
                intK += 1;
                intI += 1;
                OT_Time = 0;

            }
        }


        catch (Exception ex)
        {
        }
    }

    protected void UploadDataTableToExcel(DataTable dtRecords)
    {
        string value0 = "EMPLOYEE WISE OT" + "-" + fromdate.AddDays(0);

        string XlsPath = Server.MapPath(@"~/Add_data/DAY ATTENDANCE SUMMARY.xls");
        string attachment = string.Empty;
        if (XlsPath.IndexOf("\\") != -1)
        {
            string[] strFileName = XlsPath.Split(new char[] { '\\' });
            attachment = "attachment; filename=" + strFileName[strFileName.Length - 1];
        }
        else
            attachment = "attachment; filename=" + XlsPath;
        try
        {
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";


            //Response.Write("<table>");
            //Response.Write("<tr align='Center'>");
            //Response.Write("<td colspan='15'>");
            //Response.Write("" + value0 + " ");
            //Response.Write("</td>");
            //Response.Write("</tr>");
            //Response.Write("</table>");

            string tab = string.Empty;

            foreach (DataColumn datacol in dtRecords.Columns)
            {
                Response.Write(tab + datacol.ColumnName);
                tab = "\t";
            }
            Response.Write("\n");

            foreach (DataRow dr in dtRecords.Rows)
            {
                tab = "";
                for (int j = 0; j < dtRecords.Columns.Count; j++)
                {
                    Response.Write(tab + Convert.ToString(dr[j]));
                    tab = "\t";
                }

                Response.Write("\n");
            }

            Response.End();
        }
        catch (Exception ex)
        {
            //Response.Write(ex.Message);
        }
    }
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
    public string Right_Val(string Value, int Length)
    {
        // Recreate a RIGHT function for string manipulation
        int i = 0;
        i = 0;
        if (Value.Length >= Length)
        {
            //i = Value.Length - Length
            return Value.Substring(Value.Length - Length, Length);
        }
        else
        {
            return Value;
        }
    }

}
