﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Department_Incentive.aspx.cs" Inherits="DepartmentIncentive" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#tableCustomer').dataTable();               
            }
        }); 
    };
    </script>
      
             
     <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css" />
    <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            $('#tableCustomer').dataTable();
        });
    </script>

   <asp:UpdatePanel ID="UpdatePanel9" runat="server">
                      <ContentTemplate>
                      
 <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                       <h4><li class="active">Department Incentive</li></h4> 
                    </ol>
                </div>
       <div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
     
        
    
    <div class="page-inner">
   
                                       
            
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Department Incentive</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					
					<!-- col-sm-6 start --><div class="col-sm-6">
					
					<div class="col-sm-11"  style="padding: 0 1.4em 1.4em 1.4em !important;   border: 1px solid #e9e9e9 !important;">
                                    <div class="form-group row">
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-6">
                                    <h4>All Employee</h4>
                                    </div>
                                    </div>
                                
                                   <div class="form-group row">
                                        <label for="input-Default" class="col-sm-4 control-label">Company</label>
                                        <div class="col-sm-8">
                                            <asp:DropDownList ID="ddlCompanyCode" runat="server" class="form-control">
                                            <%--<asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="2"></asp:ListItem>--%>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-Default" class="col-sm-4 control-label">Location</label>
                                        <div class="col-sm-8">
                                            <asp:DropDownList ID="ddlLocationCode" runat="server" class="form-control">
                                           <%-- <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="2"></asp:ListItem>--%>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-Default" class="col-sm-4 control-label">Month</label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlMonth" runat="server" class="form-control">
                                            <%--<asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="2"></asp:ListItem>--%>
                                                  <asp:ListItem Value="0"> Select </asp:ListItem>     
                                                 <asp:ListItem Value="1">January</asp:ListItem>
                                                 <asp:ListItem Value="2">February</asp:ListItem>
                                                 <asp:ListItem Value="3">March</asp:ListItem>
                                                 <asp:ListItem Value="4">April</asp:ListItem>
                                                 <asp:ListItem Value="5">May</asp:ListItem>
                                                 <asp:ListItem Value="6">June</asp:ListItem>
                                                 <asp:ListItem Value="7">July</asp:ListItem>
                                                 <asp:ListItem Value="8">August</asp:ListItem>
                                                 <asp:ListItem Value="9">September</asp:ListItem>
                                                 <asp:ListItem Value="10">October</asp:ListItem>  
                                                 <asp:ListItem Value="11">November</asp:ListItem>
                                                 <asp:ListItem Value="12">December</asp:ListItem>    
                                               </asp:DropDownList>
                                            
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-Default" class="col-sm-4 control-label">Fin. Year</label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlFinancialYear" runat="server" class="form-control">
                                           <%-- <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="2"></asp:ListItem>--%>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label for="input-Default" class="col-sm-4 control-label">Wages</label>
                                        <div class="col-sm-6">
                                            <asp:DropDownList ID="ddlWages" runat="server" class="form-control">
                                            <%--<asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                            <asp:ListItem Value="1" Text="2"></asp:ListItem>--%>
                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    
                                     <div class="form-group row">
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-6">
                                    <div class="txtcenter">
                                    <asp:Button ID="btnView" class="btn btn-info"  runat="server" Text="View" />
                                    </div>
                                    </div>
                                    </div>
                                    
                              </div>     
                            </div><!-- col-sm-6 end --> 
                            
                                 
     <!-- col-sm-6 start --><div class="col-sm-6">
                                    <div class="col-sm-11"  style="padding: 0 1.4em 1.4em 1.4em !important;   border: 1px solid #e9e9e9 !important;">
                                    <div class="form-group row">
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-6">
                                    <h4>Single Employee</h4>
                                    </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-Default" class="col-sm-4 control-label">Token No</label>
                                        <div class="col-sm-6">
                                           <asp:DropDownList ID="ddlTicketNo" runat="server" class="form-control" 
                                                onselectedindexchanged="ddlTicketNo_SelectedIndexChanged" AutoPostBack="true">
                                           </asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-Default" class="col-sm-4 control-label">Existing Code</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txtExistingCode" class="form-control" runat="server" AutoPostBack="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-Default" class="col-sm-4 control-label">Employee Name</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txtEmployeeName" class="form-control" runat="server" AutoPostBack="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="input-Default" class="col-sm-4 control-label">Department</label>
                                        <div class="col-sm-6">
                                            <asp:TextBox ID="txtDepartment" class="form-control" runat="server" AutoPostBack="true"></asp:TextBox>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <div class="col-sm-4"></div>
                                        <div class="col-sm-6">
                                    <div class="txtcenter">
                                    <asp:Button ID="btnAdd" class="btn btn-success"  runat="server" Text="Add" 
                                            onclick="btnAdd_Click" />
                                    </div>
                                    </div>
                                    </div>
                                    
                                    
                               </div>    
                            </div><!-- col-sm-6 end -->                     
					  
					
					
					    <div class="form-group row">
						</div>  
						<div class="panel-body">
                             <div class="table-responsive">
    
                              <asp:Repeater ID="rptrCustomer" runat="server">
            <HeaderTemplate>
                
                  <table id="tableCustomer" class="display table" style="width: 100%; cellspacing: 0;">
                    <thead>
                        <tr>
                                                   <%-- <th>SNo</th>--%>
                                                    <th>Emp No</th>
                                                    <th>Emp Name</th>  
                                                    <th>Wages</th>
                                                    <th>Months</th>
                                                    <th>Total Days</th>
                                                    <%--<th>Edit</th>
                                                    <th>Delete</th>--%>
                        </tr>
                    </thead>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    
                   <%-- <td>
                        <%# DataBinder.Eval(Container.DataItem, "SNo")%>
                    </td>--%>
                    
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "TokenNo")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "EmpName")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "Wages")%>
                    </td>
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "Months")%>
                    </td>
                    
                     <td>
                        <%# DataBinder.Eval(Container.DataItem, "TotalDays")%>
                    </td>
                   <%-- <td>
                       <asp:LinkButton ID="lnkedit" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("TokenNo") %>' CommandName="Edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p></td>
                      </td>
                     <td>
                       <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger btn-xs"  CommandArgument='<%#Eval("TokenNo") %>' CommandName="Delete"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                    </td>--%>
                    
                </tr>
            </ItemTemplate>
            <FooterTemplate>
              
                </table>
            </FooterTemplate>
        </asp:Repeater>
                            </div>
                        </div>
						
						<!-- Button start -->
						<div class="form-group row">
						</div>
                            <div class="txtcenter">
                    <asp:Button ID="BtnSave" class="btn btn-success"  runat="server" Text="save" />
                    <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                    <asp:Button ID="BtnClear" class="btn btn-danger" runat="server" Text="Clear" />
                    </div>
                    <!-- Button End -->
                        
					     
					     
				</form>
			
			</div><!-- panel white end -->
		    </div>
		    
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
                        <!-- Dashboard start -->
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- Dashboard End -->   
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
 </div>
 
 </ContentTemplate>
      </asp:UpdatePanel>
 </div>
</asp:Content>

