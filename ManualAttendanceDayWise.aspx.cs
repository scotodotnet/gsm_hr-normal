﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;


public partial class ManualAttendanceDayWise : System.Web.UI.Page
{

    string Date;

    string SessionCcode;
    string SessionLcode;
    string SSQL;
    string SessionUserType;
    DataTable mDataSet = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
   
    DataTable table = new DataTable();
   
    DateTime fromdate;
    bool ErrFlag;
    DataTable Datacells = new DataTable();
    Boolean isPresent;
    string cc = "EVEREADY SPINING MILL";
   
    DataTable mLocalDS = new DataTable();

    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();


    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Manual Attendance";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            Date = Request.QueryString["FromDate"].ToString();

            //SessionCcode = "ESM";
            //SessionLcode = "UNIT I";
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserType = Session["UserType"].ToString();

            Fill_Manual_Day_Attd();

            Manual_Attendance_writeAttendDayWise();
        }
    }

    public void Fill_Manual_Day_Attd()
    {

        fromdate = Convert.ToDateTime(Date);



        SSQL = "";
        SSQL = "Select Distinct MA.EmpNo,EM.DeptName,EM.MachineID,EM.ExistingCode,";
        SSQL = SSQL + " (EM.FirstName + '.'+ EM.MiddleInitial) as [FirstName] from Employee_Mst EM,ManAttn_Details MA where";
        SSQL = SSQL + " EM.Compcode=MA.CompCode And EM.LocCode=MA.LocCode And EM.EmpNo=MA.EmpNo";
        SSQL = SSQL + " And EM.Compcode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "' And EM.IsActive='Yes'";
        SSQL = SSQL + " And MA.Compcode='" + SessionCcode.ToString() + "' And MA.LocCode='" + SessionLcode.ToString() + "'";
        SSQL = SSQL + " And CONVERT(VARCHAR(10), MA.AttnDate, 105) >= CONVERT(VARCHAR(10), '" + Date + "', 105)";
        //SSQL = SSQL + " And CONVERT(VARCHAR(10), MA.AttnDate, 105) <= CONVERT(VARCHAR(10), '" + Date + "', 105)";
        //SSQL = SSQL + " And EM.CatName = '" + category + "'";

        if (SessionUserType == "2")
        {
            SSQL = SSQL + " And EM.IsNonAdmin='1'";
        }
        

        
        SSQL = SSQL + " Order By EM.DeptName, EM.MachineID";

        DataTable dsEmployee = objdata.ReturnMultipleValue(SSQL);

        if (dsEmployee.Rows.Count > 0)
        {
            AutoDataTable.Columns.Add("EmpNo");
            AutoDataTable.Columns.Add("DeptName");
            AutoDataTable.Columns.Add("MachineID");
            AutoDataTable.Columns.Add("ExistingCode");
            AutoDataTable.Columns.Add("FirstName");

            for (int i = 0; i < dsEmployee.Rows.Count; i++)
            {
                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();

                AutoDataTable.Rows[i]["EmpNo"] = dsEmployee.Rows[i]["EmpNo"];
                AutoDataTable.Rows[i]["DeptName"] = dsEmployee.Rows[i]["DeptName"];
                AutoDataTable.Rows[i]["MachineID"] = dsEmployee.Rows[i]["MachineID"];
                AutoDataTable.Rows[i]["ExistingCode"] = dsEmployee.Rows[i]["ExistingCode"];
                AutoDataTable.Rows[i]["FirstName"] = dsEmployee.Rows[i]["FirstName"];

            }

            SSQL = "";
            SSQL = "select ShiftDesc,StartIN,StartIN_Days,EndIN,EndIn_Days,StartOut,StartOut_Days,EndOut,EndOut_Days";
            SSQL = SSQL + " from Shift_Mst Where CompCode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And (ShiftDesc Like '%Shift%' Or ShiftDesc Like '%GENERAL%')";

            mDataSet = null;
            mDataSet = objdata.ReturnMultipleValue(SSQL);

            if (mDataSet.Rows.Count > 0)
            {


                Datacells.Columns.Add("EmployeeNo");
                //Datacells.Columns.Add("MachineID");
                Datacells.Columns.Add("ExistingCode");
                Datacells.Columns.Add("DeptName");
                Datacells.Columns.Add("FirstName");


                DateTime dayy = Convert.ToDateTime(fromdate.AddDays(0).ToShortDateString());
                AutoDataTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));
                Datacells.Columns.Add(Convert.ToString(dayy.ToShortDateString()));
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Data not matched in nanual attendance');", true);
                ErrFlag = true;
            }


        }

    }
    public void Manual_Attendance_writeAttendDayWise()
    {

        for (int i = 0; i < AutoDataTable.Rows.Count; i++)
        {
            string Machine_ID_Str;
            string OT_Week_OFF_Machine_No;
            string Date_Value_Str;

            string Attn_Status;
            isPresent = false;
            int daycol = 4;

          

            OT_Week_OFF_Machine_No = AutoDataTable.Rows[i]["MachineID"].ToString();

            fromdate = Convert.ToDateTime(Date);

            DateTime dayy = Convert.ToDateTime(fromdate.AddDays(0).ToShortDateString());

            SSQL = "Select * from ManAttn_Details where Machine_No='" + OT_Week_OFF_Machine_No + "' And AttnDate ='" + dayy.ToShortDateString() + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

            mLocalDS = objdata.ReturnMultipleValue(SSQL);

            if (mLocalDS.Rows.Count <= 0)
            {
                Attn_Status = "";
                isPresent = true;
            }
            else
            {

                Attn_Status = mLocalDS.Rows[0]["AttnStatus"].ToString();
                isPresent = true;
            }

            if (string.IsNullOrEmpty(Attn_Status))
            {
                AutoDataTable.Rows[i][5] = "";
            }
            else if (Attn_Status == "H")
            {
                AutoDataTable.Rows[i][5] = "<b>H</b>";
            }
            else
            {
                //  Datacells.Rows[i][daycol] = "<b><span style=color:red>" + Attn_Status + "</span></b>";

                AutoDataTable.Rows[i][5] = "<b>" + Attn_Status + "</b>";
                
            }

        }
        int j = 0;
        for (int iTabRow = 0; iTabRow < AutoDataTable.Rows.Count; iTabRow++)
        {
            if (AutoDataTable.Rows[iTabRow][5].ToString() == "<b>P</b>" || AutoDataTable.Rows[iTabRow][5].ToString() == "<b>H</b>")
            {

                Datacells.NewRow();
                Datacells.Rows.Add();
                Datacells.Rows[j]["EmployeeNo"] = AutoDataTable.Rows[iTabRow]["EmpNo"];
                Datacells.Rows[j]["DeptName"] = AutoDataTable.Rows[iTabRow]["DeptName"];
                //Datacells.Rows[i]["MachineID"] = AutoDataTable.Rows[i]["MachineID"];
                Datacells.Rows[j]["ExistingCode"] = AutoDataTable.Rows[iTabRow]["ExistingCode"];
                Datacells.Rows[j]["FirstName"] = AutoDataTable.Rows[iTabRow]["FirstName"];
                Datacells.Rows[j][4] = AutoDataTable.Rows[iTabRow][5];
                j = j + 1;
            }
        }



        grid.DataSource = Datacells;
        grid.DataBind();
        string attachment = "attachment;filename=MANUAL ATTENDANCE DAYWISE.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);

        Response.Write("<table>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">" + cc + "</a>");
        Response.Write("--");
        Response.Write("<a style=\"font-weight:bold\">" + SessionLcode + "</a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">MANUAL ATTENDANCE DAYWISE REPORT &nbsp;&nbsp;&nbsp;</a>");

        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\"> FROM -" + Date + "</a>");
        Response.Write("&nbsp;&nbsp;&nbsp;");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("</table>");
        Response.Write(stw.ToString());
        Response.End();
        Response.Clear();

    }

}
