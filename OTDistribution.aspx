﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="OTDistribution.aspx.cs" Inherits="OTDistribution" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                   <h4><li class="active">OT Distribution</li>
                       <h4>
                       </h4>
                        <h4>
                       </h4>
                        <h4>
                       </h4>
                        </h4> 
                    </ol>
                </div>
<div id="main-wrapper" class="container">
<div class="row">
    <div class="col-md-12">
              
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">OT Distribution</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					
					
        
                           
					<div class="form-group row">
					
					<label for="input-Default" class="col-sm-2 control-label">IP Address</label>
					<div class="col-sm-4">
                             <asp:DropDownList ID="ddlIPAddr" class="form-control" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                    </div>
                    </div>
					<div class="form-group row">
					   <label for="input-Default" class="col-sm-2 control-label">Month</label>
						<div class="col-sm-4">
                            <asp:DropDownList ID="ddlMonth" runat="server" class="form-control">
                               <asp:ListItem Value="0">- Select -</asp:ListItem>     
                               <asp:ListItem Value="1">January</asp:ListItem>
                               <asp:ListItem Value="2">February</asp:ListItem>
                               <asp:ListItem Value="3">March</asp:ListItem>
                               <asp:ListItem Value="4">April</asp:ListItem>
                               <asp:ListItem Value="5">May</asp:ListItem>
                               <asp:ListItem Value="6">June</asp:ListItem>
                               <asp:ListItem Value="7">July</asp:ListItem>
                               <asp:ListItem Value="8">August</asp:ListItem>
                               <asp:ListItem Value="9">September</asp:ListItem>
                               <asp:ListItem Value="10">October</asp:ListItem>  
                               <asp:ListItem Value="11">November</asp:ListItem>
                               <asp:ListItem Value="12">December</asp:ListItem>    
                            </asp:DropDownList>
                        </div>
					
				    <div class="col-sm-1"></div>
				     <label for="input-Default" class="col-sm-2 control-label">Fin. Year</label>
					 <div class="col-sm-3">
                        
                         <asp:DropDownList ID="ddlFinancialYear" runat="server" class="form-control">
                         </asp:DropDownList>
                                     
					  </div>
			       </div>
			       
			        <div class="form-group row">
					    
					       <label for="input-Default" class="col-sm-2 control-label">From Date</label>
						<div class="col-sm-4">
                            <asp:TextBox ID="TxtFrmDate" class="form-control" runat="server"></asp:TextBox>
                            
                            
                              <cc1:CalendarExtender ID="CalendarExtender4" runat="server" 
                                                     CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                     TargetControlID="TxtFrmDate">
                                                 </cc1:CalendarExtender>
                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" 
                                                     FilterMode="ValidChars" 
                                                     FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                                                     TargetControlID="TxtFrmDate" ValidChars="0123456789-/">
                                                 </cc1:FilteredTextBoxExtender>
                        
                        </div>
					    
					   	<div class="col-md-1"></div>
						<label for="input-Default" class="col-sm-2 control-label">To Date</label>
						<div class="col-sm-3">
						 <asp:TextBox ID="TxtToDate" class="form-control col-md-6" AutoPostBack = "true" runat="server"></asp:TextBox>
						 
						  <cc1:CalendarExtender ID="CalendarExtender5" runat="server" 
                                                     CssClass="CalendarCSS" Enabled="true" Format="dd/MM/yyyy" 
                                                     TargetControlID="TxtToDate">
                                                 </cc1:CalendarExtender>
                                                 <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" 
                                                     FilterMode="ValidChars" 
                                                     FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom" 
                                                     TargetControlID="TxtToDate" ValidChars="0123456789-/">
                                                 </cc1:FilteredTextBoxExtender>
						 
						 
                       	</div>
				        </div> 
			       
			       
			       
					    
					    <div class="form-group row">
					    
					    <label for="input-Default" class="col-sm-2 control-label">Emp. No</label>
						<div class="col-sm-4">
                           <asp:DropDownList ID="ddlEmpNo" runat="server" class="form-control" 
                                onselectedindexchanged="ddlEmpNo_SelectedIndexChanged" AutoPostBack="true">
                         </asp:DropDownList>
                        </div>
					    
					   	<div class="col-md-1"></div>
						<label for="input-Default" class="col-sm-2 control-label">Emp. Name</label>
						<div class="col-sm-3">
						 <asp:TextBox ID="TxtEmpName" class="form-control col-md-6" AutoPostBack = "true" runat="server"></asp:TextBox>
                       	</div>
				        </div> 
                         
                         <div class="form-group row">
						<label for="input-Default" class="col-sm-2 control-label">Machine No</label>
						<div class="col-sm-4">
                            <asp:TextBox ID="TxtMachineNo" class="form-control" runat="server"></asp:TextBox>
                        </div>
					     
						<div class="col-md-1"></div>
						<label for="input-Default" class="col-sm-2 control-label">Department</label>
							<div class="col-sm-3">
                               <asp:TextBox ID="TxtDept" class="form-control" runat="server"></asp:TextBox>
							</div>
						
                         </div>
					
					    <div class="form-group row">	
				     
                         <label for="input-Default" class="col-sm-2 control-label">Wages Type</label>
						<div class="col-sm-4">
                            <asp:TextBox ID="TxtWgsType" class="form-control" runat="server"></asp:TextBox>
                        </div>
                        
                        <div class="col-md-1"></div>
						<label for="input-Default" class="col-sm-2 control-label">Designation</label>
							<div class="col-sm-3">
                               <asp:TextBox ID="TxtDesgn" class="form-control" runat="server"></asp:TextBox>
							</div>
                        
                        
						</div>
				
						
        			<div class="form-group row">
					
					<label for="input-Default" class="col-sm-2 control-label">OT Amount</label>
					<div class="col-sm-4">
                    <asp:TextBox ID="TxtOTAmt" class="form-control" runat="server"></asp:TextBox>
							</div>
                    </div>
                    <div class="form-group row">
					
					<label for="input-Default" class="col-sm-2 control-label">Rupees</label>
					<div class="col-sm-8">
                    <asp:TextBox ID="TxtRupees" class="form-control" runat="server"></asp:TextBox>
							</div>
                    </div>
				  <div class="form-group row">
					
					<label for="input-Default" class="col-sm-2 control-label">Total Cover Count</label>
					<div class="col-sm-2">
                    <asp:TextBox ID="TxtTotCount" class="form-control" runat="server"></asp:TextBox>
				    </div>
					<label for="input-Default" class="col-sm-2 control-label">Issue Count</label>
					<div class="col-sm-2">
                    <asp:TextBox ID="TxtIssueCount" class="form-control" runat="server"></asp:TextBox>
				    </div>	
					<label for="input-Default" class="col-sm-2 control-label">Balance Count</label>
					<div class="col-sm-2">
                    <asp:TextBox ID="TxtBalCount" class="form-control" runat="server"></asp:TextBox>
				    </div>			
							
                    </div>
						
						<!-- Button start -->
						<div class="form-group row">
						  <div class="col-sm-2">
						 </div>
						<div class="col-sm-4">
                     
                    <asp:FileUpload ID="fileUpload" runat="server" class="btn btn-default btn-rounded" name="filUpload"/>
                    </div>
                    
                     <div class="col-sm-1">
						 </div>
                    
                    <div class="col-sm-4">
                    <asp:Button ID="btnUpload" class="btn btn-success"  runat="server" Text="Upload" 
                            onclick="btnUpload_Click"/>
                   </div>
                    
                    </div>
                    
                    <div class="form-group row">
                    <div class="txtcenter">
                    <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Connect"/>
                                                       
                   <asp:LinkButton ID="btnClear" class="btn btn-danger" runat="server" onclick="btnClear_Click">Clear</asp:LinkButton>      
                   
                    <asp:Button ID="btnExit" class="btn btn-success"  runat="server" Text="Exit"/>
                   
                  </div>
                  
                    </div>
                    <!-- Button End -->	
						
						<div class="form-group row">
						</div>
						
						
						
       
       
						<!-- Button start -->
						<div class="form-group row">
						</div>
                           
                      </div>   
				</form>
			
			</div><!-- panel white end -->
		   
		    
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
 <!-- Dashboard start -->
 <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- Dashboard End -->   
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
</div>
 

</asp:Content>

