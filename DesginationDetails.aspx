﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DesginationDetails.aspx.cs" Inherits="DesginationDetails" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src='<%= ResolveUrl("assets/js/master_list_jquery.min.js") %>'></script>
<script src='<%= ResolveUrl("assets/js/master_list_jquery-ui.min.js") %>'></script>
<link href="<%= ResolveUrl("assets/css/master_list_jquery-ui.css") %>" rel="stylesheet" type="text/css"/>

    <script>
        $(document).ready(function () {
            $('#tableDesign').dataTable();
        });
    </script>


    <script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#tableDesign').dataTable();               
            }
        });
    };
    </script>




                   <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                     <ContentTemplate>       
                   
                              <div class="page-breadcrumb">
                                <ol class="breadcrumb container">
                                  <h4><li class="active">Desgination Details</li></h4> 
                              </ol>
                            </div>
                            
                             <div id="main-wrapper" class="container">
         <div class="row">
           <div class="col-md-12">
              
                <div class="col-md-9">
			        <div class="panel panel-white">
			          <div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Desgination</h4>
				</div>
				</div>
				        <form class="form-horizontal">
				            <div class="panel-body">
				                        <form class="form-horizontal">
			                        
				                          <div class="panel-body">
				                          
				                          
					                             <div class="form-group row">
			                                  <asp:Button ID="btnDesgn" class="btn btn-primary btn-rounded" runat="server" Text="ADD"/>
                                              <cc1:ModalPopupExtender ID="MPE1" runat="server" PopupControlID="Panel1" TargetControlID="btnDesgn"
                                              CancelControlID="" BackgroundCssClass="Background">
                                              </cc1:ModalPopupExtender>
                                              <asp:Panel ID="Panel1" runat="server" CssClass="Popup" align="center" BackColor="White" Height="269px" Width="400px" style="display:none">
				                            	<table width="100%" style="border:Solid 3px #EEE8AA; width:100%; height:100%" cellpadding="0" cellspacing="0">
                                                 <tr style="background-color:#D3D3D3">
                                                   <td colspan="2" style=" height:10%; color:White; font-weight:bold; font-size:larger" align="center">DESIGNATION</td>
                                                 </tr>
                                                 <tr>
                                                   <td align="right">Designation Name</td>
                                                   <td><asp:TextBox ID="txtDesginationName" class="form-control" runat="server" required></asp:TextBox></td>
                                                </tr>
                                                <tr>
                                                <td></td>
                                                <td>
                                                 
                                                 <asp:LinkButton ID="btnSaveDesgination" runat="server" class="btn btn-success" onclick="btnSaveDesgination_Click">Save</asp:LinkButton>        
                                                 <asp:LinkButton ID="btnClearDesgination" runat="server" class="btn btn-danger" onclick="btnClearDesgination_Click">Close</asp:LinkButton>   
                                               </td>
                                               </tr>
                                             </table>						
                                             </asp:Panel>
                                               <div class="col-sm-1"></div>
                                                </div>
                                
                                              <div class="col-md-12">
		                                      <div class="row">
                                                 <asp:Repeater ID="RepeaterDesign" runat="server" onitemcommand="RepeaterDesign_ItemCommand">
                                                 <HeaderTemplate>
                                                  <table id="tableDesign" class="display table" style="width: 100%; cellspacing: 0;">
                                                  <thead>
                                                   <tr>
                                                    <th>DesignName</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                        </tr>
                                                  </thead>
                                                 </HeaderTemplate>
                                                 <ItemTemplate>
                                                   <tr>
                    
                    
                    <td>
                        <%# DataBinder.Eval(Container.DataItem, "DesignName")%>
                    </td>
                  
                    <td>
                       <asp:LinkButton ID="lnkedit" runat="server" class="btn btn-primary btn-xs"  CommandArgument='<%#Eval("DesignNo") %>' CommandName="Edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p></td>
                      </td>
                     <td>
                       <asp:LinkButton ID="lnkDelete" runat="server" class="btn btn-danger btn-xs"  CommandArgument='<%#Eval("DesignNo") %>' CommandName="Delete"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton>
                    </td>
                    
                </tr>
                                                 </ItemTemplate>
                                                 <FooterTemplate>
              
                                                 </table>
                                                 </FooterTemplate>
                                                 </asp:Repeater>
                                                </div>
                                             </div>
                                          </div>
                               
                                        </form>
                                             
                                    </div>
                                       
		                           
			                  </form>
			                </div>
			               </div>
			               
			                <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>  
                        
                        
			               
			               
			               
			              </div>
			             </div>
			           </div>
			           
			           
			           
			           
			           
                    </ContentTemplate>
                   </asp:UpdatePanel>   


</asp:Content>

