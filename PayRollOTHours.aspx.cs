﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;


public partial class PayRollOTHours : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    BALDataAccess objdata = new BALDataAccess();
    string cc = "EVEREADY SPINING MILL";
    DataTable AutoDTable = new DataTable();
    DataTable DataCells = new DataTable();
    DataTable PayRollDT = new DataTable();
    string SSQL = "";
    DataTable dsEmployee = new DataTable();
    DataTable mDataSet = new DataTable();
    string[] Time_Minus_Value_Check;
    int shiftCount;
    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string SessionCcode;
    string SessionLcode;
    string fromdate;
    string todate;
    string cmbWages;
    DataTable mLocalDS = new DataTable();
    DateTime dtime;
    DateTime dtime1;
    double OT_Time = 0;
    DateTime date2 = new DateTime();
    string Encry_MachineID;

    bool isPresent = false;
    string Time_IN_Str;
    string Time_Out_Str;
    int j = 0;
    
    string Date_Value_Str1;
    DateTime InTime_Check;
    DateTime InTime_Check1;
    DateTime InToTime_Check;
    TimeSpan InTime_TimeSpan;
    string From_Time_Str;




    string Machine_ID_Str = "";
    string OT_Week_OFF_Machine_No = null;
    string Date_Value_Str = "";
    string Total_Time_get = "";
    string Final_OT_Work_Time_1 = "00:00";
    // Decimal Month_Mid_Join_Total_Days_Count;
    DateTime NewDate1;


    string To_Time_Str = "";
    DataTable DS_Time = new DataTable();
    DataTable DS_InTime = new DataTable();
    string Final_InTime = "";
    string Final_OutTime = "";
    string Final_Shift = "";
    DataTable Shift_DS_Change = new DataTable();
    Int32 K = 0;
    bool Shift_Check_blb = false;
    string Shift_Start_Time_Change = null;
    string Shift_End_Time_Change = null;
    string Employee_Time_Change = "";
    DateTime ShiftdateStartIN_Change = default(DateTime);
    DateTime ShiftdateEndIN_Change = default(DateTime);
    DateTime EmpdateIN_Change = default(DateTime);

    string Fin_Year;
    string Months_Full_Str;
    //string Months_Full_Str;
    string Date_Col_Str;
    string Month_Int;
    string Spin_Wages;
    string Wages;
    string Spin_Machine_ID_Str;
    string[] Att_Date_Check;
    string Att_Already_Date_Check;
    string Att_Year_Check;
    string Month_Name_Change;
    string halfPresent;

    //string Att_Year_Check = "";
    string Token_No_Get;
    TimeSpan ts4 = new TimeSpan();

    Decimal Total_Days_Count;
    double Final_Count;

    double Present_Count;
    decimal Fixed_Work_Days;
    decimal Month_WH_Count;
    double Present_WH_Count;
    double NFH_Days_Count;
    double NFH_Days_Present_Count;
    double FullNight_Shift_Count;
    double NFH_Week_Off_Minus_Days;

    Boolean NFH_Day_Check = false;
    Boolean Check_Week_Off_DOJ = false;
    int aintI = 1;
    int aintK = 1;
    int aintCol;
    int Appsent_Count;
    string SessionAdmin;

    string SessionCompanyName;
    string SessionLocationName;
    int dayCount;


    string mIpAddress_IN;
    string mIpAddress_OUT;


    string Emp_WH_Day;


    DateTime WH_DOJ_Date_Format;
    DateTime Week_Off_Date;

    DataTable Datacells = new DataTable();
    DataTable mEmployeeDS = new DataTable();

    Double NFH_Final_Disp_Days;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Payroll OT Hours";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }
            Wages = Request.QueryString["Wages"].ToString();
            fromdate = Request.QueryString["FromDate"].ToString();
            todate = Request.QueryString["ToDate"].ToString();
            //cmbWages = Request.QueryString["WagesType"].ToString();
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();

            //ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            //Date1 = Request.QueryString["dtpFromDate"].ToString();
            //Date2 = Request.QueryString["dtpToDate"].ToString();
            //ddlShiftType = Request.QueryString["ddlShiftType"].ToString();

            System.Web.UI.WebControls.DataGrid grid =
                                      new System.Web.UI.WebControls.DataGrid();

            AutoDTable.Columns.Add("DeptName");
            AutoDTable.Columns.Add("MachineID");
            AutoDTable.Columns.Add("MachineID_Enc");
            AutoDTable.Columns.Add("EmpNo");
            AutoDTable.Columns.Add("ExistingCode");
            AutoDTable.Columns.Add("FirstName");
            AutoDTable.Columns.Add("CatName");
            AutoDTable.Columns.Add("SubCatName");
            AutoDTable.Columns.Add("ShiftType");
            AutoDTable.Columns.Add("TotalHours");
            AutoDTable.Columns.Add("OTHours");
            AutoDTable.Columns.Add("Salary");


            DataCells.Columns.Add("MachineID");
            DataCells.Columns.Add("FirstName");
            DataCells.Columns.Add("CatName");
            DataCells.Columns.Add("SubCatName");
            DataCells.Columns.Add("ShiftType");


            PayRollDT.Columns.Add("Department");
            PayRollDT.Columns.Add("EmpNo");
            PayRollDT.Columns.Add("ExistingCode");
            PayRollDT.Columns.Add("FirstName");
            //PayRollDT.Columns.Add("SubCatName");
            //PayRollDT.Columns.Add(fromdate);
            //PayRollDT.Columns.Add("HrSalary");
            //PayRollDT.Columns.Add("ShiftType");
            PayRollDT.Columns.Add("HrSalary");
            PayRollDT.Columns.Add("NoHrs");
            



            //Fill_Day_Attd_Between_Dates_OT();

            writeAttend_OT_Count();

            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {


                PayRollDT.NewRow();
                PayRollDT.Rows.Add();

                PayRollDT.Rows[i]["Department"] = AutoDTable.Rows[i]["DeptName"].ToString();
                PayRollDT.Rows[i]["EmpNo"] = AutoDTable.Rows[i]["MachineID"].ToString();
                PayRollDT.Rows[i]["ExistingCode"] = AutoDTable.Rows[i]["EmpNo"].ToString();
                PayRollDT.Rows[i]["FirstName"] = AutoDTable.Rows[i]["FirstName"].ToString();
                //PayRollDT.Rows[i]["SubCatName"] = AutoDTable.Rows[i]["SubCatName"].ToString();
               // PayRollDT.Rows[i]["ShiftType"] = AutoDTable.Rows[i]["ShiftType"].ToString();
               
                PayRollDT.Rows[i]["NoHrs"] = AutoDTable.Rows[i]["OTHours"].ToString();
                PayRollDT.Rows[i]["HrSalary"] = AutoDTable.Rows[i]["Salary"].ToString();



            }

            grid.DataSource = PayRollDT;
            grid.DataBind();
            string attachment = "attachment;filename=OTReport.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);

            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();

        }
    }
    public void Fill_Day_Attd_Between_Dates_OT()
    {

        string SSQL = "";
        string Query = "";
        DataTable dtExit = new DataTable();

        SSQL = "";
        SSQL = "select isnull(DeptName,'') as [DeptName], Cast(MachineID As int) As MachineID,MachineID_Encrypt As MachineID_Enc";
        SSQL = SSQL + ",EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
        SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
        SSQL = SSQL + ",isnull(CatName,'') as [CatName], isnull(SubCatName,'') as [SubCatName]";
        SSQL = SSQL + ",isnull(ShiftType,'') as [ShiftType]";
        SSQL = SSQL + " from Employee_Mst Where Compcode='" + SessionCcode.ToString() + "'";
        SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "' And IsActive='Yes' And OTEligible='Yes' and Wages='" + Wages + "'";
        SSQL = SSQL + " Order By EmpNo asc";

        dsEmployee = objdata.ReturnMultipleValue(SSQL);
        if (dsEmployee.Rows.Count <= 0)
            return;
        int i1 = 0;

        for (int j = 0; j < dsEmployee.Rows.Count; j++)
        {
            Query = "Select EmpNo from [GSM_Epay]..EmployeeDetails where ExisistingCode='" + dsEmployee.Rows[j]["MachineID"].ToString() + "' and Ccode='" + SessionCcode.ToString() + "'";
            dtExit = objdata.RptEmployeeMultipleDetails(Query);

            if (dtExit.Rows.Count > 0)
            {
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                AutoDTable.Rows[i1][0] = dsEmployee.Rows[j]["DeptName"].ToString();
                AutoDTable.Rows[i1][1] = dtExit.Rows[0]["EmpNo"].ToString();
                AutoDTable.Rows[i1][2] = dsEmployee.Rows[j]["MachineID_Enc"].ToString();
                AutoDTable.Rows[i1][3] = dsEmployee.Rows[j]["MachineID"].ToString();
                AutoDTable.Rows[i1][4] = dsEmployee.Rows[j]["ExistingCode"].ToString();
                AutoDTable.Rows[i1][5] = dsEmployee.Rows[j]["FirstName"].ToString();
                //AutoDTable.Rows[i1][6] = dsEmployee.Rows[j]["CatName"].ToString();
                //AutoDTable.Rows[i1][7] = dsEmployee.Rows[j]["SubCatName"].ToString();
                //AutoDTable.Rows[i1][8] = dsEmployee.Rows[j]["ShiftType"].ToString();

                i1++;
            }
            else
            {
                return;
            }
            

        
        }

    }

    public void writeAttend_OT_Count()
    {
        try
        {
            Double OT_Hours = 0;
            DataColumn auto = new DataColumn();
            auto.AutoIncrement = true;
            auto.AutoIncrementSeed = 1;

            int aintCol = 4;
            int Month_Name_Change = 1;

            //string ds1 = string.Format("{dd/MM/yyyy}", Txtfrom.Text);
            date1 = Convert.ToDateTime(fromdate);
            // string dd =string.Format("{dd/MM/yyyy}",TxtTo.Text);
            date2 = Convert.ToDateTime(todate);
            dayCount = (int)((date2 - date1).TotalDays);
            int daysAdded = 0;
            Att_Already_Date_Check = "";

            AutoDTable.Columns.Add("Days");

            while (dayCount >= 0)
            {
                DateTime d1 = date1.AddDays(daysAdded);
                string date_str = Convert.ToString(d1);

                Att_Date_Check = date_str.Split('/');
                if (aintCol == 3)
                {
                    Att_Already_Date_Check = Att_Date_Check[1];
                    Att_Year_Check = Att_Date_Check[2];

                    Month_Name_Change = Convert.ToUInt16(Att_Already_Date_Check);
                    aintCol = aintCol + 1;
                }
                else
                {
                }

                dayCount = dayCount - 1;
                daysAdded = daysAdded + 1;
            }
            aintI = 0;
            aintK = 1;
            DataTable DS_WH = new DataTable();
            DataTable dt_Base = new DataTable();
            DataTable dtExit = new DataTable();
            string EmpNo = "";
            string Salary = "";
            string Query = "";
            int i1 = 0;

            SSQL = "";
            SSQL = "select isnull(DeptName,'') as [DeptName], Cast(MachineID As int) As MachineID,MachineID_Encrypt As MachineID_Enc";
            SSQL = SSQL + ",EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
            SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
            SSQL = SSQL + ",isnull(CatName,'') as [CatName], isnull(SubCatName,'') as [SubCatName]";
            SSQL = SSQL + ",isnull(ShiftType,'') as [ShiftType]";
            SSQL = SSQL + " from Employee_Mst Where Compcode='" + SessionCcode.ToString() + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode.ToString() + "' And IsActive='Yes' And OTEligible='Yes' and Wages='" + Wages + "'";
            SSQL = SSQL + " Order By EmpNo asc";

            dsEmployee = objdata.ReturnMultipleValue(SSQL);

            for (int j = 0; j < dsEmployee.Rows.Count; j++)
            {
                
                    SSQL = "select isnull(SUM(OTHours), 0) as OTHours from ManualOT where MachiineID='" + dsEmployee.Rows[j]["MachineID"].ToString() + "' and ";
                    SSQL = SSQL + "OTDate>=CONVERT(datetime,'" + fromdate + "',103) and OTDate<=CONVERT(datetime,'" + todate + "',103)";
                    DS_WH = objdata.ReturnMultipleValue(SSQL);

                    if ((DS_WH.Rows.Count > 0) && (Convert.ToDecimal(DS_WH.Rows[0]["OTHours"].ToString()) != 0))
                    {
                        OT_Time = Convert.ToDouble(DS_WH.Rows[0]["OTHours"].ToString());
                        Query = " select cast(round(OP.Base / 8,2) as decimal(18,2)) as Base,ED.EmpNo  from [GSM_Epay]..EmployeeDetails ED inner join [GSM_Epay]..SalaryMaster OP on ED.EmpNo=OP.EmpNo where BiometricID='" + dsEmployee.Rows[j]["MachineID"].ToString() + "'";
                        dt_Base = objdata.RptEmployeeMultipleDetails(Query);
                        if (dt_Base.Rows.Count > 0)
                        {
                            Salary = dt_Base.Rows[0]["Base"].ToString();
                            EmpNo = dt_Base.Rows[0]["EmpNo"].ToString();
                        }
                        else
                        {
                            Salary = "0";
                        }
                        AutoDTable.NewRow();
                        AutoDTable.Rows.Add();
                        AutoDTable.Rows[i1][0] = dsEmployee.Rows[j]["DeptName"].ToString();
                        AutoDTable.Rows[i1][1] = EmpNo;
                        AutoDTable.Rows[i1][2] = dsEmployee.Rows[j]["MachineID_Enc"].ToString();
                        AutoDTable.Rows[i1][3] = dsEmployee.Rows[j]["MachineID"].ToString();
                        AutoDTable.Rows[i1][4] = dsEmployee.Rows[j]["ExistingCode"].ToString();
                        AutoDTable.Rows[i1][5] = dsEmployee.Rows[j]["FirstName"].ToString();
                        AutoDTable.Rows[i1]["OTHours"] = OT_Time;
                        AutoDTable.Rows[i1]["TotalHours"] = "0";
                        AutoDTable.Rows[i1]["Salary"] = Salary;

                        i1++;

                    }
                    else
                    {
                        OT_Time = 0;
                    }

            }


          

            //for (int intRow = 0; intRow < AutoDTable.Rows.Count; intRow++)
            //{
            //    aintK = 1;

            //    Datacells.NewRow();
            //    Datacells.Rows.Add();

            //    for (aintCol = 0; aintCol <= 1; aintCol++)
            //    {
            //        aintK += 1;
            //    }

            //    string Emp_WH_Day;
            //    DataTable DS_WH = new DataTable();
            //    string DOJ_Date_Str;
            //    SSQL = "Select * from Employee_MST where MachineID='" + AutoDTable.Rows[intRow]["MachineID"].ToString() + "' And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
            //    DS_WH = objdata.ReturnMultipleValue(SSQL);

            //    if (DS_WH.Rows.Count != 0)
            //    {
            //        Emp_WH_Day = DS_WH.Rows[0]["WeekOff"].ToString();
            //        DOJ_Date_Str = DS_WH.Rows[0]["DOJ"].ToString();

            //    }
            //    else
            //    {
            //        Emp_WH_Day = "";
            //        DOJ_Date_Str = "";
            //    }

            //    int colIndex = aintK;

            //    //decimal Present_Count = 0;
            //    decimal Month_WH_Count = 0;
            //    //decimal Present_WH_Count = 0;
            //    //Int32 Appsent_Count = 0;
            //    //decimal Final_Count = 0;
            //    decimal Total_Days_Count = 0;
            //    decimal Month_Mid_Join_Total_Days_Count = 0;

            //    string Already_Date_Check = null;
            //    string Year_Check = null;
            //    Int32 Days_Insert_RowVal = 0;

            //    //decimal FullNight_Shift_Count = 0;
            //    // Int32 NFH_Days_Count = 0;
            //    //decimal NFH_Days_Present_Count = 0;
            //    Already_Date_Check = "";
            //    Year_Check = "";


            //    //OT_Time = 0;
            //    for (aintCol = 0; aintCol < daysAdded; aintCol++)
            //    {


            //        string Emp_Total_Work_Time_1 = "00:00";
            //        Double time_Check_dbl = 0;
            //        Spin_Machine_ID_Str = AutoDTable.Rows[intRow]["MachineID"].ToString();

            //        if (Spin_Machine_ID_Str == "3606")
            //        {
            //            string que = "Select * from Employee_Mst";
            //        }


            //        Machine_ID_Str = AutoDTable.Rows[intRow]["MachineID_Enc"].ToString();
            //        OT_Week_OFF_Machine_No = AutoDTable.Rows[intRow]["EmpNo"].ToString();

            //        if (OT_Week_OFF_Machine_No == "1019")
            //        {
            //            string QQ = "Select * from Employee_Mst";
                    
            //        }
            //        else if (OT_Week_OFF_Machine_No == "1026")
            //        {
            //            string QQ = "Select * from Employee_Mst";
            //        }

            //        NewDate1 = Convert.ToDateTime(fromdate);

            //        dtime = NewDate1.AddDays(aintCol);
            //        dtime1 = NewDate1.AddDays(aintCol).AddDays(1);
            //        Date_Value_Str = String.Format(Convert.ToString(dtime), "yyyy/MM/dd");
            //        Date_Value_Str1 = String.Format(Convert.ToString(dtime1), "yyyy/MM/dd");

            //        DataTable mLocalDS1 = new DataTable();

            //        //ss = "select LT.MachineID,Min(LT.TimeIN) as [TimeIN] from LogTime_IN LT inner join Employee_Mst EM on EM.MachineID_Encrypt=LT.MachineID";
            //        //ss = ss + " Where LT.Compcode='" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
            //        //ss = ss + "And LT.TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00" + "'";
            //        //ss = ss + "And LT.TimeIN <='" + dtime1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00" + "'";
            //        //ss = ss + "AND em.ShiftType='" + ddlshifttype.SelectedItem.Text + "' And EM.Compcode='" + SessionCcode.ToString() + "'";
            //        //ss = ss + "And EM.LocCode='" + SessionLcode.ToString() + "' And EM.IsActive='yes' Group By LT.MachineID Order By Min(LT.TimeIN)";





            //        SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
            //        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
            //        SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
            //        mLocalDS = objdata.ReturnMultipleValue(SSQL);


            //        if (mLocalDS.Rows.Count <= 0)
            //        {
            //            Time_IN_Str = "";
            //        }

            //        SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
            //        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
            //        SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT ASC";
            //        mLocalDS1 = objdata.ReturnMultipleValue(SSQL);

            //        if (mLocalDS1.Rows.Count <= 0)
            //        {
            //            Time_Out_Str = "";
            //        }


            //        if (mLocalDS.Rows.Count != 0)
            //        {

            //            InTime_Check = Convert.ToDateTime(mLocalDS.Rows[0]["TimeIN"]);

            //            InToTime_Check = InTime_Check.AddHours(2);

            //            string s1 = InTime_Check.ToString("HH:mm:ss");
            //            string s2 = InToTime_Check.ToString("HH:mm:ss");

            //            //string InTime_Check_str = String.Format("HH:mm:ss", s1);
            //            //string InToTime_Check_str = String.Format("HH:mm:ss", s2);

            //            InTime_TimeSpan = TimeSpan.Parse(s1);
            //            // From_Time_Str = Convert.ToString(InTime_TimeSpan.Hour) + ":" + Convert.ToString(InTime_TimeSpan.Minute);
            //            From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;
            //            TimeSpan InTime_TimeSpan1 = TimeSpan.Parse(s2);
            //            To_Time_Str = InTime_TimeSpan1.Hours + ":" + InTime_TimeSpan1.Minutes;

            //            SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
            //            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            //            SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + From_Time_Str + "' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + To_Time_Str + "' Order by TimeOUT Asc";
            //            DS_Time = objdata.ReturnMultipleValue(SSQL);

            //            //if (DS_Time.Rows.Count != 0)
            //            //{
            //            SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
            //            SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            //            SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + To_Time_Str + "' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
            //            DS_InTime = objdata.ReturnMultipleValue(SSQL);

            //            //if (DS_InTime.Rows.Count != 0)
            //            //{
            //            Final_InTime = mLocalDS.Rows[0][0].ToString();
            //            SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ShiftDesc like '%SHIFT%'";
            //            Shift_DS_Change = objdata.ReturnMultipleValue(SSQL);
            //            Shift_Check_blb = false;

            //            for (int k = 0; k < Shift_DS_Change.Rows.Count; k++)
            //            {
            //                DateTime DataValueStr = Convert.ToDateTime(Date_Value_Str.ToString());
            //                DateTime DataValueStr1 = Convert.ToDateTime(Date_Value_Str1.ToString());


            //                Shift_Start_Time_Change = DataValueStr.ToString("dd/MM/yyyy") + " " + Shift_DS_Change.Rows[K]["StartIN"].ToString();

            //                if (Shift_DS_Change.Rows[K]["EndIN_Days"].ToString() == "1")
            //                {
            //                    Shift_End_Time_Change = DataValueStr1.ToString("dd/MM/yyyy") + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
            //                }
            //                else
            //                {
            //                    Shift_End_Time_Change = DataValueStr.ToString("dd/MM/yyyy") + " " + Shift_DS_Change.Rows[K]["EndIN"].ToString();
            //                }




            //                ShiftdateStartIN_Change = Convert.ToDateTime(Shift_Start_Time_Change);
            //                ShiftdateEndIN_Change = Convert.ToDateTime(Shift_End_Time_Change);
            //                EmpdateIN_Change = Convert.ToDateTime(Final_InTime);

            //                if (EmpdateIN_Change >= ShiftdateStartIN_Change & EmpdateIN_Change <= ShiftdateEndIN_Change)
            //                {
            //                    Final_Shift = Shift_DS_Change.Rows[K]["ShiftDesc"].ToString();
            //                    Shift_Check_blb = true;
            //                    break;
            //                }
            //            }
            //            if (Shift_Check_blb == true)
            //            {
            //                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
            //                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            //                SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + To_Time_Str + "' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";
            //                mLocalDS = objdata.ReturnMultipleValue(SSQL);

            //                if (Final_Shift == "SHIFT2")
            //                {
            //                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
            //                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            //                    SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "13:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "03:00' Order by TimeOUT Asc";
            //                }
            //                else if (Final_Shift == "SHIFT1")
            //                {
            //                    SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
            //                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            //                    SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "00:00' And TimeIN <='" + dtime.ToString("yyyy/MM/dd") + " " + "23:59' Order by TimeIN ASC";

            //                    mLocalDS = objdata.ReturnMultipleValue(SSQL);



            //                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
            //                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            //                    SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "00:00' And TimeOUT <='" + dtime.ToString("yyyy/MM/dd") + " " + "23:59' Order by TimeOUT Asc";
            //                    mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
            //                }
            //                else
            //                {
            //                    SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
            //                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            //                    SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "17:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";
            //                    mLocalDS1 = objdata.ReturnMultipleValue(SSQL);
            //                }


            //            }
            //            else
            //            {
            //                SSQL = "Select TimeIN from LogTime_IN where MachineID='" + Machine_ID_Str + "'";
            //                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            //                SSQL = SSQL + " And TimeIN >='" + dtime.ToString("yyyy/MM/dd") + " " + "02:00' And TimeIN <='" + dtime1.ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeIN ASC";

            //                mLocalDS = objdata.ReturnMultipleValue(SSQL);

            //                SSQL = "Select TimeOUT from LogTime_OUT where MachineID='" + Machine_ID_Str + "'";
            //                SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            //                SSQL = SSQL + " And TimeOUT >='" + dtime.ToString("yyyy/MM/dd") + " " + "10:00' And TimeOUT <='" + dtime1.ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeOUT Asc";

            //                mLocalDS1 = objdata.ReturnMultipleValue(SSQL);

            //            }
            
            //        }



            //        if (mLocalDS.Rows.Count > 1)
            //        {
            //            for (int tin = 0; tin < mLocalDS.Rows.Count; tin++)
            //            {
            //                Time_IN_Str = mLocalDS.Rows[tin][0].ToString();

            //                if (mLocalDS1.Rows.Count > tin)
            //                {
            //                    Time_Out_Str = mLocalDS1.Rows[tin][0].ToString();
            //                }
            //                else if (mLocalDS1.Rows.Count > mLocalDS.Rows.Count)
            //                {
            //                    Time_Out_Str = mLocalDS1.Rows[mLocalDS1.Rows.Count - 1][0].ToString();
            //                }
            //                else
            //                {
            //                    Time_Out_Str = "";
            //                }


            //                ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
            //                if (mLocalDS.Rows.Count <= 0)
            //                {
            //                    Time_IN_Str = "";
            //                }
            //                else
            //                {
            //                    Time_IN_Str = mLocalDS.Rows[tin][0].ToString();
            //                }
            //                if (Time_IN_Str == "" || Time_Out_Str == "")
            //                {
            //                    time_Check_dbl = time_Check_dbl;
            //                }
            //                else
            //                {

            //                    date1 = System.Convert.ToDateTime(Time_IN_Str);
            //                    date2 = System.Convert.ToDateTime(Time_Out_Str);

            //                    TimeSpan ts = new TimeSpan();
            //                    ts = date2.Subtract(date1);
            //                    Total_Time_get = Convert.ToString(ts.Hours);
            //                    ts4 = ts4.Add(ts);
            //                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;



            //                    string tsfour = Convert.ToString(ts4.Minutes);
            //                    string tsfour1 = Convert.ToString(ts4.Hours);



            //                    if (Left_Val(tsfour, 1) == "-" | Left_Val(tsfour1, 1) == "-" | Emp_Total_Work_Time_1 == "0:0")
            //                    {
            //                        Emp_Total_Work_Time_1 = "00:00";
            //                    }

            //                    if (Left_Val(Total_Time_get, 1) == "-")
            //                    {
            //                        date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
            //                        ts = date2.Subtract(date1);
            //                        ts = date2.Subtract(date1);
            //                        Total_Time_get = ts.Hours.ToString();
            //                        time_Check_dbl = Convert.ToInt16(Total_Time_get.ToString());
            //                        //time_Check_dbl = Convert.ToInt16(time_Check_dbl) + Convert.ToInt16(Total_Time_get.ToString());
            //                        Emp_Total_Work_Time_1 = ts.Hours + ":" + ts.Minutes;
            //                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

            //                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
            //                        {
            //                            Emp_Total_Work_Time_1 = "00:00";
            //                        }
            //                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
            //                        {
            //                            Emp_Total_Work_Time_1 = "00:00";
            //                        }
            //                    }
            //                    else
            //                    {
            //                        time_Check_dbl = Convert.ToInt16(time_Check_dbl) + Convert.ToInt16(Total_Time_get.ToString());
            //                    }

            //                }
            //            }
            //        }
            //        else
            //        {
            //            TimeSpan ts4 = new TimeSpan();
            //            ts4 = Convert.ToDateTime(string.Format("{0:hh\\:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
            //            if (mLocalDS.Rows.Count <= 0)
            //            {
            //                Time_IN_Str = "";
            //            }
            //            else
            //            {
            //                Time_IN_Str = mLocalDS.Rows[0][0].ToString();
            //            }
            //            for (int tout = 0; tout <= mLocalDS1.Rows.Count - 1; tout++)
            //            {
            //                if (mLocalDS1.Rows.Count <= 0)
            //                {
            //                    Time_Out_Str = "";
            //                }
            //                else
            //                {
            //                    Time_Out_Str = mLocalDS1.Rows[0][0].ToString();
            //                }
            //            }

            //            if (Time_IN_Str == "" || Time_Out_Str == "")
            //            {
            //                time_Check_dbl = time_Check_dbl;
            //            }

            //            else
            //            {

            //                date1 = System.Convert.ToDateTime(Time_IN_Str);
            //                date2 = System.Convert.ToDateTime(Time_Out_Str);

            //                TimeSpan ts = new TimeSpan();
            //                ts = date2.Subtract(date1);
            //                Total_Time_get = Convert.ToString(ts.Hours);
            //                ts4 = ts4.Add(ts);
            //                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;



            //                string tsfour = Convert.ToString(ts4.Minutes);
            //                string tsfour1 = Convert.ToString(ts4.Hours);



            //                if (Left_Val(tsfour, 1) == "-" | Left_Val(tsfour1, 1) == "-" | Emp_Total_Work_Time_1 == "0:0")
            //                {
            //                    Emp_Total_Work_Time_1 = "00:00";
            //                }

            //                if (Left_Val(Total_Time_get, 1) == "-")
            //                {
            //                    date2 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
            //                    ts = date2.Subtract(date1);
            //                    ts = date2.Subtract(date1);
            //                    Total_Time_get = ts.Hours.ToString();
            //                    time_Check_dbl = Convert.ToInt16(Total_Time_get.ToString());
            //                    //time_Check_dbl = Convert.ToInt16(time_Check_dbl) + Convert.ToInt16(Total_Time_get.ToString());
            //                    Emp_Total_Work_Time_1 = ts.Hours + ":" + ts.Minutes;
            //                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

            //                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
            //                    {
            //                        Emp_Total_Work_Time_1 = "00:00";
            //                    }
            //                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
            //                    {
            //                        Emp_Total_Work_Time_1 = "00:00";
            //                    }
            //                }
            //                else
            //                {
            //                    time_Check_dbl = Convert.ToInt16(time_Check_dbl) + Convert.ToInt16(Total_Time_get.ToString());
            //                }

            //            }
            //        }


            //        string Emp_Total_Work_Time;
            //        string Final_OT_Work_Time;
            //        string Final_OT_Work_Time_Val;
            //        //String Emp_Total_Work_Time;
            //        string Employee_Week_Name;
            //        string Assign_Week_Name;
            //        //mLocalDS == null;

            //        Emp_Total_Work_Time = Emp_Total_Work_Time_1;

            //        Employee_Week_Name = Convert.ToDateTime(Date_Value_Str).DayOfWeek.ToString();
            //        SSQL = "Select WeekOff from Employee_Mst where MachineID='" + OT_Week_OFF_Machine_No + "'";
            //        mLocalDS = objdata.ReturnMultipleValue(SSQL);
            //        if (mLocalDS.Rows.Count <= 0)
            //        {
            //            Assign_Week_Name = "";
            //        }
            //        else
            //        {
            //            Assign_Week_Name = mLocalDS.Rows[0]["WeekOff"].ToString();
            //        }
            //        Boolean NFH_Day_Check = false;
            //        DateTime NFH_Date;
            //        DateTime DOJ_Date_Format;
            //        DateTime Date_Value = new DateTime();

            //        if (Employee_Week_Name.ToString() == Assign_Week_Name.ToString())
            //        {
            //            //Parthi OT Time Start
            //            if (time_Check_dbl >= 9)
            //            {
            //                isPresent = true;
            //                halfPresent = "0";
            //                OT_Time = OT_Time + (time_Check_dbl - 1);

            //            }
            //            else
            //            {
            //                isPresent = false;
            //                halfPresent = "0";
            //            }
            //            //OT Time End
            //        }
            //        else
            //        {
            //            double Calculate_Attd_Work_Time = 0;
            //            double Calculate_Attd_Work_Time_half = 0;

            //            SSQL = "Select * from Employee_MST where MachineID='" + OT_Week_OFF_Machine_No + "' and Calculate_Work_Hours <> ''";
            //            mLocalDS = objdata.ReturnMultipleValue(SSQL);
            //            if (mLocalDS.Rows.Count <= 0)
            //            {
            //                Calculate_Attd_Work_Time = 9;
            //            }
            //            else
            //            {
            //                //Calculate_Attd_Work_Time = (!string.IsNullOrEmpty(mLocalDS.Rows[0]["Calculate_Work_Hours"]) ? mLocalDS.Rows[0]["Calculate_Work_Hours"] : 0);
            //                if (Calculate_Attd_Work_Time == 0 || Calculate_Attd_Work_Time == null)
            //                {
            //                    Calculate_Attd_Work_Time = 9;
            //                }
            //                else
            //                {
            //                    Calculate_Attd_Work_Time = 9;
            //                }

            //            }

            //            Calculate_Attd_Work_Time_half = 4.0;
            //            halfPresent = "0";

            //            //Parthi OT Time Start

            //            if (Wages == "STAFF salary")
            //            {
            //                if (time_Check_dbl >= 10)
            //                {
            //                    isPresent = true;
            //                    halfPresent = "0";
            //                    OT_Time = OT_Time + (time_Check_dbl - 9);

            //                }
            //                else
            //                {
            //                    isPresent = false;
            //                    halfPresent = "0";
            //                }
            //            }
            //            else
            //            {
            //                if (time_Check_dbl >= Calculate_Attd_Work_Time)
            //                {
            //                    isPresent = true;
            //                    halfPresent = "0";
            //                    OT_Time = OT_Time + (time_Check_dbl - 8);

            //                }
            //                else
            //                {
            //                    isPresent = false;
            //                    halfPresent = "0";
            //                }
            //            }
                      
            //            //OT Time End
                       
            //        }


            //        //WeekOff  Work  Days
            //        string Attn_Date_Day = DateTime.Parse(Date_Value_Str).DayOfWeek.ToString();
            //        if (Emp_WH_Day == Attn_Date_Day)
            //        {
            //                Month_WH_Count = Month_WH_Count + 1;
            //                if (isPresent == true)
            //                {
            //                    if (halfPresent == "1")
            //                    {
            //                        Present_WH_Count = Convert.ToDouble(Present_WH_Count + 0.5);
            //                    }
            //                    else if (halfPresent == "0")
            //                    {
            //                        Present_WH_Count = Present_WH_Count + 1;
            //                    }

            //                }
                     
            //        }

            //        if (isPresent == true)
            //        {

            //            if (halfPresent == "1")
            //            {
            //                Present_Count = Convert.ToDouble(Present_Count) + 0.5;
                           
            //            }
            //            else if (halfPresent == "0")
            //            {
            //                Present_Count = Present_Count + 1;
                          
            //            }

            //        }
            //        else
            //        {
            //            Appsent_Count = Appsent_Count + 1;
            //        }


            //        colIndex += shiftCount;
            //        aintK += 1;
            //        Total_Days_Count = Total_Days_Count + 1;

            //    } //For End

            //    AutoDTable.Rows[intRow]["OTHours"] = OT_Time;
            //    AutoDTable.Rows[intRow]["TotalHours"] = "0";
            //    AutoDTable.Rows[intRow]["Salary"] = "0";




            //    //if (time_Check_dbl.ToString() != "")
            //    //{
            //    //    AutoDTable.Rows[intRow]["TotalHours"] = time_Check_dbl.ToString();
            //    //    AutoDTable.Rows[intRow]["Salary"] = "0";
            //    //    decimal Total_Time = Convert.ToInt16(time_Check_dbl.ToString());
            //    //    date1 = Convert.ToDateTime(fromdate);
            //    //    date2 = Convert.ToDateTime(todate);
            //    //    int dayCounts = (int)((date2 - date1).TotalDays) + 1;
            //    //    //decimal Normal_Time = dayCounts * 8;
            //    //    //decimal Fix_Time = dayCounts * 12;
            //    //    //decimal Fix_Time1 = dayCounts * 9;
            //    //    //Total_Time = Total_Time / dayCounts;

            //    //    decimal Normal_Time;
            //    //    if (cmbWages == "Gents hostel salary")
            //    //    {
            //    //        if (Total_Time > 0)
            //    //        {

            //    //            Normal_Time = daysAdded * 12;
            //    //            OT_Time = Total_Time - Normal_Time;
            //    //            //  AutoDTable.Rows[intRow]["OTHours"] = OT_Time.ToString();
            //    //            if (OT_Time < 0)
            //    //            {
            //    //                OT_Time = 0;
            //    //            }
            //    //        }
            //    //        else
            //    //        {
            //    //            //AutoDTable.Rows[intRow]["OTHours"] = "0
            //    //            OT_Time = OT_Time + 0;
            //    //        }
            //    //    }
            //    //    else
            //    //    {
            //    //        if (Total_Time > 0)
            //    //        {
            //    //            Normal_Time = daysAdded * 8;
            //    //            OT_Time = Total_Time - Normal_Time;
            //    //            //AutoDTable.Rows[intRow]["OTHours"] = OT_Time.ToString();

            //    //            if (OT_Time < 0)
            //    //            {
            //    //                OT_Time = 0;
            //    //            }
            //    //        }
            //    //        else
            //    //        {
            //    //            OT_Time = OT_Time + 0;
            //    //        }
            //    //    }
            //    //    AutoDTable.Rows[intRow]["OTHours"] = OT_Time;
            //    //}




            //    Final_Count = Present_Count;
            //    //if (NFH_Days_Count == 0)
            //    //{
            //    //    NFH_Days_Present_Count = 0;
            //    //}
            //    string Token_No;
            //    string Machine_ID_Str_Excel;
            //    DataTable Token_DS = new DataTable();



            //    // Machine_ID_Str_Excel = Datacells.Rows[aintI][1].ToString();

            //    //SSQL = "Select * from Employee_Mst where MachineID='" + Datacells.Rows[aintI][1].ToString() + "' and Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And IsActive='Yes'";
            //    //Token_DS = objdata.ReturnMultipleValue(SSQL);
            //    //if (Token_DS.Rows.Count != 0)
            //    //{
            //    //    Token_No_Get = Token_DS.Rows[0]["ExistingCode"].ToString();
            //    //    Int32 Fixed_Work_Days = 0;
            //    //}

            //    if (Wages == "STAFF salary" | Wages == "Watch & Ward")
            //    {
            //        Fixed_Work_Days = Month_Mid_Join_Total_Days_Count - Month_WH_Count;
            //        Final_Count = Final_Count - Present_WH_Count;
            //        Final_Count = Final_Count - NFH_Days_Present_Count;
            //        Present_WH_Count = Present_WH_Count + NFH_Days_Present_Count;
            //    }
            //    else
            //    {
            //        //Fixed_Work_Days = 0;
            //        //Final_Count = Final_Count - Present_WH_Count;
            //        //Final_Count = Final_Count - NFH_Days_Present_Count;
            //        //Present_WH_Count = Present_WH_Count;

            //        Fixed_Work_Days = Month_Mid_Join_Total_Days_Count - Month_WH_Count;
            //        Final_Count = Final_Count - Present_WH_Count;
            //        Final_Count = Final_Count - NFH_Days_Present_Count;
            //        Present_WH_Count = Present_WH_Count + NFH_Days_Present_Count;

            //    }



            //    string NFH_Year_Str = "0";
            //    string NFH_Month_Str = "";
            //    string NFH_Rule_OLD = "";
            //    int NFH_Display_Days = 0;
            //    string NFH_Double_Wages_Rule = "";
            //    DataTable mDataSet = new DataTable();
            //    NFH_Year_Str = Convert.ToString(date1.Year);
            //    NFH_Month_Str = Convert.ToString(date1.Month);

            //    if (Final_Count != 0)
            //    {
            //        //    //PayRollDT.NewRow();
            //        //PayRollDT.Rows.Add();


            //        //PayRollDT.Rows[i]["Total Hours"] = 
            //        //PayRollDT.Rows[i]["HrSalary"] = Final_Wages;
            //        //PayRollDT.Rows[i]["Total"] = TotalAmt;

            //    }

            //    aintI = aintI + 1;
            //    Total_Days_Count = 0;
            //    Final_Count = 0;
            //    Appsent_Count = 0;
            //    Present_Count = 0;
            //    Fixed_Work_Days = 0;
            //    Month_WH_Count = 0;
            //    Present_WH_Count = 0;
            //    NFH_Days_Count = 0;
            //    NFH_Days_Present_Count = 0;
            //    OT_Time = 0;
            //    OT_Hours = 0;
                
            //}


        }


        catch (Exception e)
        {
        }
    }



    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }


    public string Right_Val(string Value, int Length)
    {
        // Recreate a RIGHT function for string manipulation
        int i = 0;
        i = 0;
        if (Value.Length >= Length)
        {
            //i = Value.Length - Length
            return Value.Substring(Value.Length - Length, Length);
        }
        else
        {
            return Value;
        }
    }
   

}