﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Datatablesample.aspx.cs" Inherits="Datatablesample" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

 <div id="main-wrapper" class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-heading clearfix">
                                    <h4 class="panel-title">Basic example</h4>
                                </div>
                                <div class="panel-body">
                                   <div class="table-responsive">
                                   
                                   <asp:Repeater ID="Repeater1" runat="server">
                                       <HeaderTemplate>
                                   
                                        <table id="myTable" class="display table" style="width: 100%; cellspacing: 0;">
                                         <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Location Name</th>
                                                    <th>User Name</th>
                                                    <th>User Type</th>
                                                </tr>
                                            </thead>
                                        </HeaderTemplate>
                                         <ItemTemplate>
                                           <tbody>
                                                 <tr>
                                                    <td><asp:CheckBox ID="CheckBox1" runat="server" /></td>
                                                    <td><%# Eval("LocCode") %></td>
                                                    <td><%# Eval("UserName") %></td>
                                                    <td><%# Eval("UserType") %></td>
                                                </tr>
                                            </tbody>
                                        </ItemTemplate>
                                         <FooterTemplate>
                                      </table>
                                 </FooterTemplate> 
                                   
                                    
                                   </asp:Repeater>
                                       
                                    </div>
                                </div>
                            </div>
             
    </div>
    
    </div>
    </div>
</asp:Content>

