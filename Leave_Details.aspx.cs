﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;


public partial class Leave_Details : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    ManualAttendanceClass objManual = new ManualAttendanceClass();
    bool ErrFlag = false;
    string SSQL;
    string MonthDays_Get;
    DateTime fromdate;
    DateTime todate;
    DataTable dtdDisplay = new DataTable();
    string deptname;

    DataTable mDataSet = new DataTable();
  

    int leave_Mst; 
    int leave; 
    int Remaining;
    static string tt;
    static string Auto_ID;
    static string id;
    string leavetype1;
    string Timing = "";
    int HOUR;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
        

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Leave Details";
                HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("LeaveManagement"));
                li.Attributes.Add("class", "droplink active open");

                //Dropdown_Company();
                //Dropdown_Location();
                DropDown_TokenNumber();
                //DropDown_LeaveTypeMst();
                DisplayLeaveReg();
                Financial_Year();
               
            }
        }
    }

    public void DisplayLeaveReg()
    {
        DataTable dtDisplay = new DataTable();
        SSQL = "select * from Leave_Register_Mst";
        dtdDisplay = objdata.ReturnMultipleValue(SSQL);
        rptrCustomer.DataSource = dtdDisplay;
        rptrCustomer.DataBind();
      

     }

    public void Financial_Year()
    {
        String CurrentYear1;
        int CurrentYear;
        CurrentYear1 = DateTime.Now.Year.ToString();
        CurrentYear = Convert.ToUInt16(CurrentYear1.ToString());
        if (DateTime.Now.Month >= 1 && DateTime.Now.Month <= 3)
        {
            CurrentYear = CurrentYear - 1;
        }
        for (int i = 0; i < 1; i++)
        {
            tt = CurrentYear1;
            CurrentYear = CurrentYear - 1;
            string cy = Convert.ToString(CurrentYear);
            CurrentYear1 = cy;

        }
    }

    

    public void DropDown_LeaveTypeMst()
    {
        DataTable dt = new DataTable();
        dt = objdata.dropdown_LeaveType();
        ddlLeaveType.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["LeaveType"] = "- select -";
        dt.Rows.InsertAt(dr, 0);
        for (int i = 0; i < dt.Rows.Count; i++) 
        {
            ddlLeaveType.Items.Add(dt.Rows[i]["LeaveType"].ToString());
        }

    }
    public void DropDown_TokenNumber()
    {
        DataTable dt = new DataTable();
        dt = objdata.dropdown_TokenNumber(SessionCcode, SessionLcode);
        ddlTicketno.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["EmpName"] = "- select -";
        dt.Rows.InsertAt(dr, 0);
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            ddlTicketno.Items.Add(dt.Rows[i]["EmpName"].ToString());
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {

        string Encrpt_ID = UTF8Encryption(ddlTicketno.SelectedItem.Text);

        DataTable dtbLeave = new DataTable();
        string s = ddlTicketno.SelectedItem.Text;
        string[] delimiters = new string[] { "-->" };
        string[] items = s.Split(delimiters, StringSplitOptions.None);
        string EmpNo = items[0];
        string EmpName = items[1];

        SSQL = "select * from Leave_Register_Mst where EmpNo='" + EmpNo.Trim() + "' and convert(datetime,FromDate,103)=convert(datetime,'" + txtFromDate.Text + "',103) and convert(datetime,ToDate,103)=convert(datetime,'" + txtToDate.Text + "',103)";
        dtbLeave = objdata.ReturnMultipleValue(SSQL);
        if (dtbLeave.Rows.Count > 0)
        {
            SSQL = "delete from Leave_Register_Mst where EmpNo='" + EmpNo.Trim() + "' and convert(datetime,FromDate,103)=convert(datetime,'" + txtFromDate.Text + "',103) and convert(datetime,ToDate,103)=convert(datetime,'" + txtToDate.Text + "',103)";
            dtbLeave = objdata.ReturnMultipleValue(SSQL);
        }

        SSQL = "Insert Into Leave_Register_Mst(CompCode,LocCode,EmpNo,ExistingCode,Machine_No,Machine_Encrypt,EmpName";
        SSQL = SSQL + ",FromDate,ToDate,TotalDays,LeaveType) Values('" + SessionCcode + "'";
        SSQL = SSQL + ",'" + SessionLcode + "','" + EmpNo.Trim() + "','" + EmpNo.Trim() + "','" + EmpNo.Trim() + "'";
        SSQL = SSQL + ",'" + UTF8Encryption(ddlTicketno.SelectedItem.Text) + "','" + txtEmpName.Text + "','" + txtFromDate.Text + "','" + txtToDate.Text + "'";
        SSQL = SSQL + ",'" + txtTotalDays.Text + "','" + ddlLeaveType.SelectedItem.Text + "')";
       
        dtbLeave = objdata.ReturnMultipleValue(SSQL);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Added sucessFully');", true);
        DisplayLeaveReg();
        Clear();
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear();
    }
    
    public void Clear()
    {
        ddlTicketno.Enabled = true;
        txtExistingCode.Text = "";
        txtFromDate.Text = "";
        txtLeaveDesc.Text = "";
        ddlTicketno.SelectedIndex = 0;
        //txtMachineNo.Text = "";
        txtToDate.Text = "";
        txtTotalDays.Text = "";
        ddlLeaveType.SelectedIndex = 0;
        txtEmpName.Text = "";
        ddlTicketno.Items.Clear();
        ddlLeaveType.Items.Clear();

        btnSave.Text = "Save";

        DropDown_TokenNumber();
        DropDown_LeaveTypeMst();
       
     
    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
       string[] commandArgs = e.CommandArgument.ToString().Split(new char[] { ',' });
       id = commandArgs[0];
       Auto_ID = commandArgs[1];
  
       
        switch (e.CommandName)
        {
            case ("Delete"):
                DeleteRepeaterData(id, Auto_ID);
                break;
            case ("Edit"):
                EditRepeaterData(id, Auto_ID);
                break;
        }
    }

    private void EditRepeaterData(string id,string Auto_ID)
    {
       
        DataTable Dtd = new DataTable();
        SSQL = "select * from Leave_Register_Mst where EmpNo='"+ id +"' and Auto_ID='"+ Auto_ID +"'";
        Dtd = objdata.ReturnMultipleValue(SSQL);
        if (Dtd.Rows.Count > 0)
        {
            ddlTicketno.SelectedItem.Text = Dtd.Rows[0]["EmpNo"].ToString();
            txtExistingCode.Text = Dtd.Rows[0]["ExistingCode"].ToString();
            txtEmpName.Text = Dtd.Rows[0]["EmpName"].ToString();
            txtFromDate.Text = Dtd.Rows[0]["FromDate"].ToString();
            txtToDate.Text = Dtd.Rows[0]["ToDate"].ToString();
            txtTotalDays.Text = Dtd.Rows[0]["TotalDays"].ToString();
            ddlLeaveType.SelectedItem.Text = Dtd.Rows[0]["LeaveType"].ToString();
            txtLeaveDesc.Text = Dtd.Rows[0]["LeaveDesc"].ToString();
            DisplayLeaveReg();
            btnSave.Text = "Update";
            ddlTicketno.Enabled = false;      
        }

    }
    private void DeleteRepeaterData(string id, string Auto_ID)
    {
      
        DataTable Dtd = new DataTable();
        SSQL = "Delete Leave_Register_Mst where EmpNo='" + id + "' and Auto_ID = '" + Auto_ID + "'";
        Dtd = objdata.ReturnMultipleValue(SSQL);
        DisplayLeaveReg();
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Delete sucessFully');", true);
        ErrFlag = true;
    }


    protected void ddlTicketno_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dted = new DataTable();

        string s = ddlTicketno.SelectedItem.Text;
        string[] delimiters = new string[] { "-->" };
        string[] items = s.Split(delimiters, StringSplitOptions.None);
        string EmpNo = items[0];
        string EmpName = items[1];

        dted = objdata.Manual_Data(EmpNo);
        if (dted.Rows.Count > 0)
        {
            txtExistingCode.Text = dted.Rows[0]["ExistingCode"].ToString();
            txtEmpName.Text = dted.Rows[0]["FirstName"].ToString();
            string gender = dted.Rows[0]["Gender"].ToString();
       
           // if (gender == "Male")
           // {

           //     ddlLeaveType.Items.Clear();

           //     DataTable dt = new DataTable();
           //     dt = objdata.DropDown_LeaveTypeMale();
           //     ddlLeaveType.DataSource = dt;
           //     DataRow dr = dt.NewRow();
           //     dr["LeaveType"] = "- select -";
           //     dt.Rows.InsertAt(dr, 0);
           //     for (int i = 0; i < dt.Rows.Count; i++)
           //     {
           //         ddlLeaveType.Items.Add(dt.Rows[i]["LeaveType"].ToString());
           //     }
           //}
          
        }
    }
    protected void txtToDate_TextChanged(object sender, EventArgs e)
    {
        fromdate = Convert.ToDateTime(txtFromDate.Text);
        todate = Convert.ToDateTime(txtToDate.Text);
        int dayCount = (int)((todate - fromdate).TotalDays) + 1;
        txtTotalDays.Text = Convert.ToString(dayCount.ToString());
    }

    private static string UTF8Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }
 
}
     