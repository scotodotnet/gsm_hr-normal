﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;



public partial class TabAccordian : System.Web.UI.Page
{
    string SessionCcode;
    string SessionLcode;
    string SessionAdmin;
    DataTable dtdDisplay = new DataTable();
    BALDataAccess objdata = new BALDataAccess();

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        //SessionCompanyName = Session["CompanyName"].ToString();
        //SessionLocationName = Session["LocationName"].ToString();
        
        if (!IsPostBack)
        {
            CreateUserDisplay();
            
        }

    }

  
    public void CreateUserDisplay()
    {
        dtdDisplay = objdata.UserCreationDisplay(SessionCcode, SessionLcode);
        if (dtdDisplay.Rows.Count > 0)
        {
            rptrCustomer.DataSource = dtdDisplay;
            rptrCustomer.DataBind();
        }

    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        string id = Convert.ToString(e.CommandArgument);
        switch (e.CommandName)
        {
            case ("Delete"):
                //DeleteRepeaterData(id);
                break;
            case ("Edit"):
                //EditRepeaterData(id);
                break;
        }
    }

}
