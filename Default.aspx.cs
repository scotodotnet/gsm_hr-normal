﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.Data.SqlClient;
using System.IO;


public partial class _Default : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    UserRegistrationClass objuser = new UserRegistrationClass();
    string ccode;
    string lcode;

    string Company_Code;
    string Location_Code;
    string[] Company;
    string[] Location;
    string SSQL;



    string EmpNo1;
    string EmpNo2;
    string EmpNo3;
    string EmpNo4;
    string EmpNo5;
    string EmpNo6;
    string EmpNo7;
    string EmpNo8;
    string EmpNo9;
    string EmpNo10;
    string EmpNo11;
    string EmpNo12;
    string EmpNo13;
  
    protected void Page_Load(object sender, EventArgs e)
    {
        Session.Remove("UserId");
        Session.Remove("Isadmin");
        if (!IsPostBack)
        {

            Load_company();
            Load_Location();
            Module_Login_Table(sender, e);
            //Check Module Login Table
            MailDate_Check();

        }

    }
    public void MailDate_Check()
    {
        DataTable dtd = new DataTable();
        DataTable MailDT = new DataTable();
        string ss = "";
        DateTime Currentdate = new DateTime();

        ss = "SELECT GETDATE() AS CurrentDateTime";
        dtd = objdata.ReturnMultipleValue(ss);

        Currentdate = Convert.ToDateTime(dtd.Rows[0]["CurrentDateTime"]);
        ss = "Select *from MailDetail_Tbl where Date_Str='" + Currentdate.ToString("dd/MM/yyyy") + "'";
        MailDT = objdata.ReturnMultipleValue(ss);

        if (MailDT.Rows.Count == 0)
        {
            ss = "Update MailDetail_Tbl set Status='0',Date_Str=''";
            objdata.ReturnMultipleValue(ss);
        }
        else
        {
        }


    }
    public void Load_company()
    {
        DataTable dt = new DataTable();
        dt = objdata.Dropdown_Company();
        SSQL = "Select Distinct CM.CompCode,CM.CompCode + ' - ' + CompName as Cname from Company_Mst CM inner join Location_Mst LM on CM.CompCode=LM.CompCode where LM.LocCode='" + ddlLocationCode.SelectedValue + "' ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);


        ddlCompanyCode.DataSource = dt;
        //DataRow dr = dt.NewRow();
        //dr["CompCode"] = "Company Name";
        //dr["Cname"] = "Company Name";
        //dt.Rows.InsertAt(dr, 0);
        ddlCompanyCode.DataTextField = "Cname";
        ddlCompanyCode.DataValueField = "CompCode";
        ddlCompanyCode.DataBind();
    }

    public void Load_Location()
    {
       DataTable dtblocation = new DataTable();
       SSQL = "select * from Location_Mst";
       dtblocation=objdata.ReturnMultipleValue(SSQL);
       ddlLocationCode.DataSource = dtblocation;
       DataRow dr = dtblocation.NewRow();
       dr["LocCode"] = "Location Name";
       dr["LocName"] = "Location Name";
       dtblocation.Rows.InsertAt(dr, 0);
       ddlLocationCode.DataTextField = "LocCode";
       ddlLocationCode.DataValueField = "LocCode";
       ddlLocationCode.DataBind();
      

        //DataTable dt = new DataTable();
        //dt = objdata.dropdown_loc(ddlCompanyCode.SelectedValue);
        //ddlLocationCode.DataSource = dt;
        //DataRow dr = dt.NewRow();
        //dr["LocCode"] = "Location Name";
        //dr["Location"] = "Location Name";
        //dt.Rows.InsertAt(dr, 0);
        //ddlLocationCode.DataTextField = "Location";
        //ddlLocationCode.DataValueField = "LocCode";
        //ddlLocationCode.DataBind();
        //Load_company();

    }

    protected void btnlogin_Click(object sender, EventArgs e)
    {

        bool ErrFlag = false;
        Session["SessionSpay"] = "GSM_Spay";
        Session["SessionEpay"] = "GSM_Epay";

        string Company_Code = ddlCompanyCode.SelectedItem.Text;
        string Location_Code = ddlLocationCode.SelectedItem.Text;
        string[] Company = Company_Code.Split('-');
        string[] Location = Location_Code.Split('-');
        ccode = Company[0].ToString().Trim();
        lcode = Location[0].ToString().Trim();


        if (ddlCompanyCode.SelectedItem.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Your Company Name');", true);
            ErrFlag = true;
        }
        else if (ddlLocationCode.SelectedItem.Text == "Location Name")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Your Location Name');", true);
            ErrFlag = true;
        }

        else if (txtusername.Text != " " && txtpwd.Text != " ")
        {

            DataTable dtbulogin = new DataTable();
            string pwd_Str = UTF8Encryption(txtpwd.Text);
            //SSQL = "select*from User_Login where UserName='" + txtusername.Text + "' and CompCode='" + ccode + "' and LocCode ='" + lcode + "' and UserType='0'";
            SSQL = "select*from [HR_Rights]..MstUsers where UserCode='" + txtusername.Text + "' And Password='" + pwd_Str + "' and CompCode='" + ccode + "' and LocationCode ='" + lcode + "'";
            dtbulogin = objdata.ReturnMultipleValue(SSQL);
            if (dtbulogin.Rows.Count > 0)
            {
                if (txtusername.Text.ToUpper().ToString() == "Scoto".ToUpper().ToString())
                {

                    Session["CMP"] = "Company Manster";
                    Session["LCA"] = "Scoto";
                    Session["Data"] = "1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1";
                    Session["Ccode"] = Company[0];
                    ccode = Company[0];
                    Session["CompanyName"] = Company[1];
                    Session["Lcode"] = Location[0];
                    lcode = Location[0];
                    Session["LocationName"] = Location[0];
                    Session["Isadmin"] = txtusername.Text;
                    Session["UserType"] = "0";
                    Session["Isadmin"] = txtusername.Text;
                    Response.Redirect("Company_Master.aspx");
                }
                else
                {
                    string date_1 = "";
                    string dtserver = "";

                    date_1 = (24 + "-" + 02 + "-" + 2022).ToString();

                    string constr = ConfigurationManager.AppSettings["ConnectionString"];
                    SqlConnection con = new SqlConnection(constr);
                    string Query = "";

                    con.Open();
                    Query = "Select convert(varchar,getdate(),105) as ServerDate";
                    SqlCommand cmd_Server_Date = new SqlCommand(Query, con);
                    dtserver = (cmd_Server_Date.ExecuteScalar()).ToString();

                    con.Close();

                    if (Convert.ToDateTime(dtserver) < Convert.ToDateTime(date_1))
                    {
                        if (dtbulogin.Rows[0]["IsAdmin"].ToString() == "1")
                        {
                            Session["Data"] = "1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1";
                            Session["Ccode"] = Company[0];
                            ccode = Company[0];
                            Session["UserType"] = dtbulogin.Rows[0]["IsAdmin"].ToString();
                            Session["CompanyName"] = Company[1];
                            Session["Lcode"] = Location[0];
                            lcode = Location[0];
                            Session["LocationName"] = Location[0];
                            Session["Isadmin"] = txtusername.Text;
                            Response.Redirect("Dash_Board.aspx");
                        }
                        else
                        {
                            string Module_ID_Encrypt = Encrypt("1");
                            SSQL = "select*from [HR_Rights]..Company_Module_User_Rights where ModuleID='" + Module_ID_Encrypt + "' And UserName='" + txtusername.Text + "'and CompCode='" + ccode + "' and LocCode ='" + lcode + "'";
                            dtbulogin = objdata.ReturnMultipleValue(SSQL);
                            if (dtbulogin.Rows.Count != 0)
                            {
                                Session["Data"] = "1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1";
                                Session["Ccode"] = Company[0];
                                ccode = Company[0];
                                Session["UserType"] = dtbulogin.Rows[0]["IsAdmin"].ToString();
                                Session["CompanyName"] = Company[1];
                                Session["Lcode"] = Location[0];
                                lcode = Location[0];
                                Session["LocationName"] = Location[0];
                                Session["Isadmin"] = txtusername.Text;
                                Response.Redirect("Dash_Board.aspx");
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Rights set to the User');", true);
                                ErrFlag = true;
                            }
                        }
                    }
                    else
                    {

                    }
                }

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Name and password Incorrect ');", true);
                ErrFlag = true;

                //DataTable dtbLogin = new DataTable();
                //string pwd = UTF8Encryption(txtpwd.Text);
                //dtbLogin = objdata.CheckUserCreation(ccode, lcode, txtusername.Text, pwd);
                //if (dtbLogin.Rows.Count > 0)
                //{
                //    DataTable dtbRights = new DataTable();
                //    dtbRights = objdata.Login_Rights(ccode, lcode, txtusername.Text);
                //    if (dtbRights.Rows.Count > 0)
                //    {
                //        Session["Data"] = dtbRights.Rows[0]["Data"].ToString();
                //        Session["Ccode"] = Company[0];
                //        ccode = Company[0];
                //        Session["CompanyName"] = Company[1];
                //        Session["Lcode"] = Location[0];
                //        lcode = Location[0];
                //        Session["LocationName"] = Location[1];
                //        Session["UserType"] = "1";
                //        Session["Isadmin"] = txtusername.Text;
                //        Response.Redirect("Dash_Board.aspx");
                //    }
                //    else
                //    {
                //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No admin Rights');", true);
                //        ErrFlag = true;
                //    }
                //}

                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Name and password Incorrect ');", true);
                //    ErrFlag = true;
                //}

            }
        }
    }
    

    //public static String UTF8Encryption(String originalPassword)
    //{
    //    UTF8Encoding encoder = new UTF8Encoding();
    //    MD5 md5 = new MD5CryptoServiceProvider();
    //    Byte[] hashedbytes = md5.ComputeHash(encoder.GetBytes(originalPassword));
    //    return BitConverter.ToString(hashedbytes).Replace("-", "").ToLower();
    //}


    private static string UTF8Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
   }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }


    protected void ddlCompanyCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Location();
    }
    protected void ddlLocationCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_company();
    }

    private string Encrypt(string clearText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(clearBytes, 0, clearBytes.Length);
                    cs.Close();
                }
                clearText = Convert.ToBase64String(ms.ToArray());
            }
        }
        return clearText;
    }

    private string Decrypt(string cipherText)
    {
        string EncryptionKey = "MAKV2SPBNI99212";
        byte[] cipherBytes = Convert.FromBase64String(cipherText);
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (MemoryStream ms = new MemoryStream())
            {
                using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                {
                    cs.Write(cipherBytes, 0, cipherBytes.Length);
                    cs.Close();
                }
                cipherText = Encoding.Unicode.GetString(ms.ToArray());
            }
        }
        return cipherText;
    }
    private void Module_Login_Table(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT_Module = new DataTable();
        query = "Select * from [HR_Rights]..Module_Open_User";
        DT_Module = objdata.RptEmployeeMultipleDetails(query);
        if (DT_Module.Rows.Count != 0)
        {
            ddlCompanyCode.SelectedValue = DT_Module.Rows[0]["CompCode"].ToString();
            ddlLocationCode.SelectedValue = DT_Module.Rows[0]["LocCode"].ToString();
            txtusername.Text = DT_Module.Rows[0]["UserName"].ToString();
            txtpwd.Text = UTF8Decryption(DT_Module.Rows[0]["Password"].ToString()).ToString();

            query = "Delete from [HR_Rights]..Module_Open_User";
            objdata.RptEmployeeMultipleDetails(query);

            btnlogin_Click(sender, e);

        }
    }

    public static String s_hex_md5(String originalPassword)
    {
        UTF8Encoding encoder = new UTF8Encoding();
        MD5 md5 = new MD5CryptoServiceProvider();

        Byte[] hashedbytes = md5.ComputeHash(encoder.GetBytes(originalPassword));
        return BitConverter.ToString(hashedbytes).Replace("-", "").ToLower();
    }

    public static String Decodemd5(String originalPassword)
    {
        UTF8Encoding encoder = new UTF8Encoding();
        MD5 md5 = new MD5CryptoServiceProvider();

        Byte[] hashedbytes = md5.ComputeHash(encoder.GetBytes(originalPassword));
        return BitConverter.ToString(hashedbytes).Replace("-", "").ToLower();
    }
}
