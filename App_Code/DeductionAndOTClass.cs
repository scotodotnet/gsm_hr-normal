﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for DeductionAndOTClass
/// </summary>
public class DeductionAndOTClass
{
	public DeductionAndOTClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    private string _Ccode;

    public string Ccode
    {
        get { return _Ccode; }
        set { _Ccode = value; }
    }
    private string _Lcode;

    public string Lcode
    {
        get { return _Lcode; }
        set { _Lcode = value; }
    }

    private string _CompanyCode;

    public string CompanyCode
    {
        get { return _CompanyCode; }
        set { _CompanyCode = value; }
    }

    private string _LocationCode;

    public string LocationCode
    {
        get { return _LocationCode; }
        set { _LocationCode = value; }
    }

    private string _Month;

    public string Month
    {
        get { return _Month; }
        set { _Month = value; }
    }

    private string _FinancialYear;

    public string FinancialYear
    {
        get { return _FinancialYear; }
        set { _FinancialYear = value; }
    }

    private string _Wages;

    public string Wages
    {
        get { return _Wages; }
        set { _Wages = value; }
    }

    private string _MachineID;

    public string MachineID
    {
        get { return _MachineID; }
        set { _MachineID = value; }
    }

    private string _Name;

    public string Name
    {
        get { return _Name; }
        set { _Name = value; }
    }

    private string _TokenNumber;

    public string TokenNumber
    {
        get { return _TokenNumber; }
        set { _TokenNumber = value; }
    }

    private string _FromDate;

    public string FromDate
    {
        get { return _FromDate; }
        set { _FromDate = value; }
    }

    private string _ToDate;

    public string ToDate
    {
        get { return _ToDate; }
        set { _ToDate = value; }
    }

    private string _AllowOtherOne;
    public string AllowOtherOne
    {
        get { return _AllowOtherOne; }
        set { _AllowOtherOne = value; }
    }

    private string _AllowOtherTwo;
    public string AllowOtherTwo
    {
        get { return _AllowOtherTwo; }
        set { _AllowOtherTwo = value; }
    }

    private string _Advance;
    public string Advance
    {
        get { return _Advance; }
        set { _Advance = value; }
    }


    private string _Canteen;
    public string Canteen
    {
        get { return _Canteen; }
        set { _Canteen = value; }
    }

    private string _LWF;
    public string LWF
    {
        get { return _LWF; }
        set { _LWF = value; }
    }

    private string _PTax;
    public string PTax
    {
        get { return _PTax; }
        set { _PTax = value; }
    }

    private string _DeductionOtherOne;
    public string DeductionOtherOne
    {
        get { return _DeductionOtherOne; }
        set { _DeductionOtherOne = value; }
    }

    private string _DeductionOtherTwo;
    public string DeductionOtherTwo
    {
        get { return _DeductionOtherTwo; }
        set { _DeductionOtherTwo = value; }
    }

    private string _HAllowed;
    public string HAllowed
    {
        get { return _HAllowed; }
        set { _HAllowed = value; }
    }

    private string _OTHours;
    public string OTHours
    {
        get { return _OTHours; }
        set { _OTHours = value; }
    }

    private string _CanteenDaysMinus;
    public string CanteenDaysMinus
    {
        get { return _CanteenDaysMinus; }
        set { _CanteenDaysMinus = value; }
    }

    private string _LOPDays;
    public string LOPDays
    {
        get { return _LOPDays; }
        set { _LOPDays = value; }
    }
    
}
