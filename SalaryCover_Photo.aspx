﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SalaryCover_Photo.aspx.cs" Inherits="SalaryCover_Photo" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                       <h4><li class="active">Salary Cover</li></h4> 
                    </ol>
                </div>
                
                        
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
    <div class="page-inner">
     <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Salary Cover</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
				<div class="form-group row">
<!-- col-12 start --><div class="col-sm-12">
				      <!-- col-6 start -->
				      <div class="col-sm-6">
			 <div class="form-group row">
						<label for="input-Default" class="col-sm-3 control-label">company Code<span class="mandatory">*</span></label>
						<div class="col-sm-6">
                            <asp:DropDownList ID="ddlCompanyCode" runat="server" class="form-control">
                                <%--<asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                <asp:ListItem Value="5" Text="5"></asp:ListItem>--%>
                                </asp:DropDownList>
						</div>
						
               </div>                     
					<div class="form-group row">
						 <label for="input-Default" class="col-sm-3 control-label">Location Code</label>
							<div class="col-sm-6">
                               <asp:DropDownList ID="ddlLocationCode" runat="server" class="form-control">
                              <%--  <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                <asp:ListItem Value="5" Text="5"></asp:ListItem>--%>
                                </asp:DropDownList>
							</div>
							
						</div>
					    
					    <div class="form-group row">
					    
				       <label for="input-Default" class="col-sm-3 control-label">IP Address</label>
							<div class="col-sm-6">
                                 <asp:DropDownList ID="ddlIPAddress" runat="server" class="form-control">
                                <%--<asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                <asp:ListItem Value="2" Text="New User"></asp:ListItem>
                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                <asp:ListItem Value="5" Text="5"></asp:ListItem>--%>
                                </asp:DropDownList>
							</div>
                         </div>
                         
                          <div class="form-group row">
					    
				       <label for="input-Default" class="col-sm-3 control-label">Month</label>
							<div class="col-sm-6">
                                 <asp:DropDownList ID="ddlMonth" runat="server" class="form-control">
                               <%-- <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                <asp:ListItem Value="2" Text="New User"></asp:ListItem>
                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                <asp:ListItem Value="5" Text="5"></asp:ListItem>--%>
                                </asp:DropDownList>
							</div>
							</div>
							
							<div class="form-group row">
							<label for="input-Default" class="col-sm-3 control-label">Fin. Year</label>
							<div class="col-sm-6">
                                 <asp:DropDownList ID="ddlFinancialYear" runat="server" class="form-control">
                               <%-- <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                <asp:ListItem Value="2" Text="New User"></asp:ListItem>
                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                <asp:ListItem Value="5" Text="5"></asp:ListItem>--%>
                                </asp:DropDownList>
							</div>
                         </div>
					             </div><!-- col-6 end -->
					             
					             <!-- col-6 start -->
					             <div class="col-sm-5">
					               <h4>Employee Photo</h4>
                                    <div class="form-group row">
							            <div class="image-crop">
                                                <img src="assets/images/employee.jpg" style="width: 100%; height: 100%;" alt="">
                                        </div>
						            </div>
						           </div><!-- col-6 end -->
						    <div class="form-group row">
						    <div class="col-sm-12">
						    <div class="col-sm-6">
						    
						    <div class="form-group row">
						    <label for="input-Default" class="col-sm-3 control-label">From Date</label>
							<div class="col-sm-6">
							 <asp:TextBox ID="txtFromDate" class="form-control date-picker" name="ctl00$ContentPlaceHolder1$tin_date" type="text" runat="server"></asp:TextBox> 
						   <%-- <input name="ctl00$ContentPlaceHolder1$tin_date" type="text" id="Text4" class="form-control date-picker">--%>       
						   </div>
						   </div>
						   
						   <div class="form-group row">
						    <label for="input-Default" class="col-sm-3 control-label">To Date</label>
							<div class="col-sm-6">
							 <asp:TextBox ID="txtToDate" class="form-control date-picker" name="ctl00$ContentPlaceHolder1$tin_date" type="text" runat="server"></asp:TextBox> 
						    <%--<input name="ctl00$ContentPlaceHolder1$tin_date" type="text" id="Text5" class="form-control date-picker">       --%>
						   </div>
						   </div>
						   
						   <div class="form-group row">
						    <label for="input-Default" class="col-sm-3 control-label">Mobile No</label>
							<div class="col-sm-6">
						    <asp:TextBox ID="txtMobileNumber" class="form-control" runat="server"></asp:TextBox>       
						   </div>
						   </div>
						   
						    <div class="form-group row">
						    <label for="input-Default" class="col-sm-3 control-label">Wages Type</label>
							<div class="col-sm-6">
						    <asp:TextBox ID="txtWagesType" class="form-control" runat="server"></asp:TextBox>       
						   </div>
						   </div>
						   
						   <div class="form-group row">
						    <label for="input-Default" class="col-sm-3 control-label"><b>RUPEES</b></label>
							<div class="col-sm-6">
						    <asp:TextBox ID="txtRupees" class="form-control" runat="server"></asp:TextBox>       
						   </div>
						   </div>
						   
						   
						   </div><!-- col-6 end -->
						   
						   
						   <div class="col-sm-6">
						   
						   <div class="form-group row">
						   <label for="input-Default" class="col-sm-3 control-label">Token No</label>
							<div class="col-sm-6">
						    <asp:TextBox ID="txtTokenNumber" class="form-control" runat="server"></asp:TextBox>       
						   </div> 
						   </div>
						   
						   <div class="form-group row">
						   <label for="input-Default" class="col-sm-3 control-label">Emp. Name</label>
							<div class="col-sm-6">
						    <asp:TextBox ID="txtEmployeeName" class="form-control" runat="server"></asp:TextBox>       
						   </div> 
						   </div>
						   
						   <div class="form-group row">
						   <label for="input-Default" class="col-sm-3 control-label">Emp. Dept</label>
							<div class="col-sm-6">
						    <asp:TextBox ID="txtEmployeeDepartment" class="form-control" runat="server"></asp:TextBox>       
						   </div> 
						   </div>
						   
						   <div class="form-group row">
						   <label for="input-Default" class="col-sm-3 control-label">Desg</label>
							<div class="col-sm-6">
						    <asp:TextBox ID="txtDesgination" class="form-control" runat="server"></asp:TextBox>       
						   </div> 
						   </div>
						   
						   <div class="form-group row">
						   <label for="input-Default" class="col-sm-3 control-label"><b>NET AMOUNT</b></label>
							<div class="col-sm-6">
						    <asp:TextBox ID="txtNetAmount" class="form-control" runat="server"></asp:TextBox>       
						   </div> 
						   </div>
						         
				           </div><!-- col-6 end -->
				            
				            </div>
				            </div><!-- col-12 end -->
				                        
				  </div><!-- col-12 over all end -->
				  </div>
				      
				      
                                  
					
					 
					<!-- Button start -->
						<div class="form-group row">
						</div>
                            <div class="txtcenter">
                    <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" />
                    <%--<asp:Button ID="Button1" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg" runat="server" Text="View" />--%>
                    <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" />
                    </div>
                    <!-- Button End -->
					
					      
                        
				</form>
			
			</div><!-- panel white end -->
		    </div>
		    
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
 <!-- Dashboard start -->
 <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                            
                        </div>  
                        <!-- Dashboard End -->   
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
 </div>
  </div>
</asp:Content>

