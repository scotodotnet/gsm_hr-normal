﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;


public partial class Datatablesample : System.Web.UI.Page
{
    DataTable dtdDisplay = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    UserCreationClass objUsercreation = new UserCreationClass();


    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {

           CreateUserDisplay();
        }
    }

    public void CreateUserDisplay()
    {
        //dtdDisplay = objdata.UserCreationDisplay(objUsercreation);
        if (dtdDisplay.Rows.Count > 0)
        {
            Repeater1.DataSource = dtdDisplay;
            Repeater1.DataBind();

        }

    }


}
