﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;

public partial class Shift_Master : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    BALDataAccess objdata = new BALDataAccess();
    string SSQL;
    bool ErrFlag = false;
    static int SNo;
    DataTable dtdMstShift = new DataTable();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Shift Master";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("masterpage"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionCompanyName = Session["CompanyName"].ToString();
            SessionLocationName = Session["LocationName"].ToString();
            if (!IsPostBack)
            {
                
               
            }
        }
    }

  
    //protected void ddlTicketNo_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    DataTable dt = new DataTable();
    //    DataTable dted = new DataTable();
    //    string s = ddlTicketNo.SelectedItem.Text;
    //    string[] delimiters = new string[] { "-->" };
    //    string[] items = s.Split(delimiters, StringSplitOptions.None);
    //    string ss = items[0];
    //    string ss1 = items[1];
    //    txtEmpName.Text = ss1.ToString();
    //    txtEmpName.Text = items[1];
    
    //    dted = objdata.Manual_Data(ss.ToString());
    //    if (dted.Rows.Count > 0)
    //    {
    //     txtEmpName.Text = dted.Rows[0]["FirstName"].ToString();
    //     lblShowDept.Text = dted.Rows[0]["DeptName"].ToString();

       
    //    }
    //  }
    protected void txtEmpNo_TextChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        string SQL = "select EmpNo,ExistingCode,FirstName,DeptName,CatName from Employee_Mst where MachineID='" + txtEmpNo.Text + "'";
        dt = objdata.RptEmployeeMultipleDetails(SQL);
        if (dt.Rows.Count > 0)
        {
            txtEmpName.Text = dt.Rows[0]["FirstName"].ToString();
            lblShowDept.Text = dt.Rows[0]["DeptName"].ToString();

            string empcategory = dt.Rows[0]["CatName"].ToString();

            if (empcategory.ToString() == "STAFF")
            {
                ddlCategory.SelectedIndex = 1;
            }
            else
            {
                ddlCategory.SelectedIndex = 2;
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('This Employee No not Matching');", true);
        }

    }

    //public void DropDown_TokenNumber()
    //{
    //    DataTable dt = new DataTable();
    //    string SQL = "Select MachineID,MachineID + ' --> ' + FirstName as EmpName from Employee_MST where IsActive='Yes' and OTEligible='Yes' ";
    //    SQL = SQL + "And CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' order by CAST (MachineID as decimal(18,0)) asc";
    //    dt = objdata.RptEmployeeMultipleDetails(SQL);
    //    ddlTicketNo.DataSource = dt;
    //    DataRow dr = dt.NewRow();
    //    dr["EmpName"] = "- select -";
    //    dt.Rows.InsertAt(dr, 0);
    //    for (int i = 0; i < dt.Rows.Count; i++)
    //    {
    //        ddlTicketNo.Items.Add(dt.Rows[i]["EmpName"].ToString());
    //    }
    //}
   
    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (txtEmpNo.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Employee No');", true);
            ErrFlag = true;
        }

        else if (ddlCategory.SelectedItem.Text == "- select -")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select Employee Category');", true);
            ErrFlag = true;
        }

        else if (txtDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter Date');", true);
            ErrFlag = true;
        }
        else if (txtOTHours.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter OT Hours');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            DataTable dt = new DataTable();
            string ErrMsg = "Insert";
            string SQL = "";
            //string s = ddlTicketNo.SelectedItem.Text;
            //string[] delimiters = new string[] { "-->" };
            //string[] items = s.Split(delimiters, StringSplitOptions.None);
            //string ss = items[0];
            //string ss1 = items[1];

            SQL = "select * from ManualOT where MachiineID='" + txtEmpNo.Text + "' and OTDate=convert(datetime,'" + txtDate.Text + "',103)";
            dt = objdata.RptEmployeeMultipleDetails(SQL);
            if (dt.Rows.Count > 0)
            {
                SQL = "delete from ManualOT where MachiineID='" + txtEmpNo.Text + "' and OTDate=convert(datetime,'" + txtDate.Text + "',103)";
                dt = objdata.RptEmployeeMultipleDetails(SQL);
                ErrMsg = "Update";
            }

            SQL = "insert into ManualOT(MachiineID,EmpName,CatName,DeptName,OTDate,OTHours,Ccode,Lcode) ";
            SQL = SQL + "Values('" + txtEmpNo.Text + "','" + txtEmpName.Text + "','" + ddlCategory.SelectedItem.Text + "','" + lblShowDept.Text + "',";
            SQL = SQL + "convert(datetime,'" + txtDate.Text + "',103),'" + txtOTHours.Text + "','" + SessionCcode + "','" + SessionLcode + "')";
            objdata.RptEmployeeMultipleDetails(SQL);

            if (ErrMsg == "Insert")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Added Successfully...,');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Update Successfully...,');", true);
            }


            Clear();

        }
    }
    public void Clear()
    {
        
        ddlCategory.SelectedIndex = 0;
        lblShowDept.Text = "";
        txtEmpName.Text = "";
        txtEmpNo.Text = "";
        txtOTHours.Text = "";
    }


    protected void BtnClear_Click(object sender, EventArgs e)
    {
        Clear();
    }



    
}
