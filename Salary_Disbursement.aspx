﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Salary_Disbursement.aspx.cs" Inherits="Salary_Disbursement" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                       <h4><li class="active">Salary Disbursement</li></h4> 
                    </ol>
                </div>
                
                        
<div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
     
        
    
    <div class="page-inner">
   
                                       
            
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">Salay Disbursement</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					
					<div class="form-group row">
<!-- col-sm-12 start--><div class="col-sm-12">
<!-- col-sm-6 start--><div class="col-sm-6">
					
					<div class="form-group row">
						<label for="input-Default" class="col-sm-4 control-label">Company Code<span class="mandatory">*</span></label>
						<div class="col-sm-6">
                          <asp:DropDownList ID="ddlCompanyCode" runat="server" class="form-control">
                               <%-- <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                <asp:ListItem Value="5" Text="5"></asp:ListItem>--%>
                                </asp:DropDownList>
                            
						</div>
						</div>
						
						<div class="form-group row">
						<label for="input-Default" class="col-sm-4 control-label">Pay Period</label>
						<div class="col-sm-6">
                          <asp:DropDownList ID="ddlPayPeriod" runat="server" class="form-control">
                               <%-- <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                <asp:ListItem Value="5" Text="5"></asp:ListItem>--%>
                                </asp:DropDownList>
                            
						</div>
						</div>
						
						 <div class="form-group row">
						<label for="input-Default" class="col-sm-4 control-label">Pay From</label>
						<div class="col-sm-6">
                           
						
						<input name="ctl00$ContentPlaceHolder1$tin_date" type="text" id="Text1" class="form-control date-picker">
                        </div>
						</div>
						
						 <div class="form-group row">
						<label for="input-Default" class="col-sm-4 control-label">Pay To</label>
						<div class="col-sm-6">
						<asp:TextBox ID="txtPayTo" class="form-control date-picker" name="ctl00$ContentPlaceHolder1$tin_date" type="text" runat="server"></asp:TextBox> 
					<%--	<input name="ctl00$ContentPlaceHolder1$tin_date" type="text" id="Text2" class="form-control date-picker">--%>
                        </div>
						</div>
						
						</div><!-- col-sm-6 end-->
						
  <!-- col-sm-6 start--><div class="col-sm-6">
						
						<div class="form-group row">
				        <label for="input-Default" class="col-sm-4 control-label">Location Code<span class="mandatory">*</span></label>
						<div class="col-sm-6">
                            <asp:DropDownList ID="ddlLocationCode" runat="server" class="form-control">
                               <%-- <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                <asp:ListItem Value="5" Text="5"></asp:ListItem>--%>
                                </asp:DropDownList>
                            
						</div>
               </div>                     
					  
					  <div class="form-group row">
						<label for="input-Default" class="col-sm-4 control-label">Wages Type</label>
						<div class="col-sm-6">
                          <asp:DropDownList ID="ddlWagesType" runat="server" class="form-control">
                                <%--<asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                <asp:ListItem Value="5" Text="5"></asp:ListItem>--%>
                                </asp:DropDownList>
                            
						</div>
						</div>
						
                        <div class="form-group row">
						<label for="input-Default" class="col-sm-4 control-label">Payout From</label>
						<div class="col-sm-6">
							<asp:TextBox ID="txtPayoutFrom" class="form-control date-picker" name="ctl00$ContentPlaceHolder1$tin_date" type="text" runat="server"></asp:TextBox> 
<%--						<input name="ctl00$ContentPlaceHolder1$tin_date" type="text" id="Text9" class="form-control date-picker">
--%>                        </div>
						</div>
						
						<div class="form-group row">
						<label for="input-Default" class="col-sm-4 control-label">Payout To</label>
						<div class="col-sm-6">
					      <asp:TextBox ID="txtPayOutTo" class="form-control date-picker" name="ctl00$ContentPlaceHolder1$tin_date" type="text" runat="server"></asp:TextBox> 
					<%--	<input name="ctl00$ContentPlaceHolder1$tin_date" type="text" id="Text3" class="form-control date-picker">--%>
                        </div>
						</div>
					
					</div><!-- col-sm-6 end-->
					</div><!-- col-sm-12 end-->
					</div>
					
					<div class="form-group row">
<!-- col-sm-12 start--><div class="col-sm-12">
<!-- col-sm-6 start--><div class="col-sm-6">
                       
                       <div class="form-group row">
						<div class="col-sm-4"></div>
						<div class="col-sm-6"><h4>IP Address Details</h4></div>
						</div>
						
					  </div><!-- col-sm-6 end-->
					  
<!-- col-sm-6 start--><div class="col-sm-6">
                       
                       <div class="form-group row">
						<div class="col-sm-4"></div>
						<div class="col-sm-6"><asp:Button ID="btnGetMachine" class="btn btn-info"  runat="server" Text="Get Machine"/></div>
						</div>
						

                       </div><!-- col-sm-6 end-->     					  
					  
					</div><!-- col-sm-12 end-->
					</div>
					
					    <div class="form-group row">
						</div>  
						<div class="table-responsive scroll-table">
                                        <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Table Header</th>
                                                    <th>Table Header</th>
                                                    <th>Table Header</th>
                                                    <th>Table Header</th>
                                                    <th>Table Header</th>
                                                    <th>Table Header</th>
                                                 </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                </tr>
                                                <tr>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                </tr>
                                                
                                                <tr>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                    <td>Table cell</td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                          
                          
                          <div class="form-group row"></div>
                       <div class="form-group row">
<!-- col-sm-12 start--><div class="col-sm-12">
<!-- col-sm-6 start--><div class="col-sm-6">

                           <div class="col-sm-1"></div>
                           <div class="form-group row">
						   <label for="input-Default" class="col-sm-4 control-label">Total Amount</label>
						   <div class="col-sm-6">
						   <asp:TextBox ID="txtTotalAmount" class="form-control" runat="server"></asp:TextBox>       
						   </div>
						   </div>
						   
						   <div class="col-sm-1"></div>
						   <div class="form-group row">
						   <label for="input-Default" class="col-sm-4 control-label">Payout Cover</label>
						   <div class="col-sm-6">
						   <asp:TextBox ID="txtPayOutCover" class="form-control" runat="server"></asp:TextBox>       
						   </div>
						   </div>
						   
						   
						   
						   <div class="col-sm-1"></div>
						   <div class="form-group row">
						   <label for="input-Default" class="col-sm-4 control-label">Balance Cover</label>
						   <div class="col-sm-6">
						   <asp:TextBox ID="txtBalanceCover" class="form-control" runat="server"></asp:TextBox>       
						   </div>
						   </div>
						   
						  
					  </div><!-- col-sm-6 end-->
					  
<!-- col-sm-6 start--><div class="col-sm-6">
                           
                           <div class="col-sm-1"></div>
						   <div class="form-group row">
						   <label for="input-Default" class="col-sm-4 control-label">Payout Amount</label>
						   <div class="col-sm-6">
						   <asp:TextBox ID="txtPayoutAmount" class="form-control" runat="server"></asp:TextBox>       
						   </div>
						   </div>
						   
						   
                           <div class="col-sm-1"></div>
						   <div class="form-group row">
						   <label for="input-Default" class="col-sm-4 control-label">Balance Amount</label>
						   <div class="col-sm-6">
						   <asp:TextBox ID="txtBalanceAmount" class="form-control" runat="server"></asp:TextBox>       
						   </div>
						   </div>
						   
						   <div class="form-group row">
						   <div class="col-sm-6"></div>
						   <div class="col-sm-5">
						   <asp:Button ID="btnPrint" class="btn btn-info"  runat="server" Text="Print"/>       
						   </div>
						   </div>
						

                       </div><!-- col-sm-6 end-->     					  
					  
					</div><!-- col-sm-12 end-->
					</div>                                    
						
                        <!-- Button start -->
						<div class="form-group row">
						</div>
                            <div class="txtcenter">
                    <asp:Button ID="btnImport" class="btn btn-success"  runat="server" Text="Import" />
                    <asp:Button ID="btnProcess" class="btn btn-primary"  runat="server" Text="Process" />
                    <asp:Button ID="btnReport" class="btn btn-info" runat="server" Text="Report" />
                    <asp:Button ID="btnAbstract" class="btn btn-danger" runat="server" Text="Abstract" />
                    </div>
                    <!-- Button End -->	
					     
					     
				</form>
			
			</div><!-- panel white end -->
		    </div>
		    
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
 <!-- Dashboard start -->
 <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
                        <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- Dashboard End -->   
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
 </div>
  </div>

</asp:Content>

