<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="User_Creation1.aspx.cs" Inherits="UserCreation" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

   <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.0/css/jquery.dataTables.css" />
    <script type="text/javascript" charset="utf8" src="//code.jquery.com/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.0/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function () {
            $('#tableCustomer').dataTable();
        });
    </script>





        <div class="page-breadcrumb">
                    <ol class="breadcrumb container">
                       <h4><li class="active">User Creation</li></h4> 
                    </ol>
                </div>
 
 <div id="main-wrapper" class="container">
 <div class="row">
    <div class="col-md-12">
    <div class="page-inner">
            <div class="col-md-9">
			<div class="panel panel-white">
			<div class="panel panel-primary">
				<div class="panel-heading clearfix">
					<h4 class="panel-title">User Creation</h4>
				</div>
				</div>
				<form class="form-horizontal">
				<div class="panel-body">
					
					<div class="form-group row">
						<label for="input-Default" class="col-sm-2 control-label">company<span class="mandatory">*</span></label>
						<div class="col-sm-4">
                            <asp:DropDownList ID="ddlCompany" runat="server" class="form-control">
                            </asp:DropDownList>
						</div>
						<div class="col-md-1"></div>
				        <label for="input-Default" class="col-sm-2 control-label">User Name</label>
						<div class="col-sm-3">
                            <asp:TextBox ID="txtUserName" class="form-control" runat="server" required></asp:TextBox>
                       </div>
                   </div>                     
					<div class="form-group row">
						 <label for="input-Default" class="col-sm-2 control-label">Location</label>
							<div class="col-sm-4">
                               <asp:DropDownList ID="ddlLocation" runat="server" class="form-control">
                               </asp:DropDownList>
							</div>
							<div class="col-sm-1"></div> 
							<label for="input-Default" class="col-sm-2 control-label">Password</label>
							<div class="col-sm-3">
                                 <asp:TextBox ID="txtPassword" class="form-control" runat="server" TextMode="Password" required></asp:TextBox>
							</div>
						</div>
					    
					    <div class="form-group row">
					    
				       <label for="input-Default" class="col-sm-2 control-label">User Type</label>
							<div class="col-sm-4">
                                 <asp:DropDownList ID="ddlUserType" runat="server" class="form-control">
                                 <asp:ListItem Value="1" Text="- select -"></asp:ListItem>
                                 <asp:ListItem Value="2" Text="Admin"></asp:ListItem>
                                 <asp:ListItem Value="3" Text="Non-Admin"></asp:ListItem>
                                </asp:DropDownList>
							</div>
                         </div>  
                         
                        
					<!-- Button start -->
						<div class="form-group row">
						</div>
                         <div class="txtcenter">
                               <asp:Button ID="btnSave" class="btn btn-success"  runat="server" Text="Save" 
                                    onclick="btnSave_Click" />
                   
                                <asp:Button ID="btnCancel" class="btn btn-danger" runat="server" Text="Cancel" />
                         </div>
                    <!-- Button End -->
					
					       <div class="form-group row">
						  </div>
						  
						  
						  
						  
	     
                                  <div class="panel-body">
                                   <div class="table-responsive">
                                   <asp:Repeater ID="rptrCustomer" runat="server">
                                  
                                   <HeaderTemplate>              
                                   
                                    <table id="tableCustomer" class="display table" style="width: 100%; cellspacing: 0;">
                                        <thead>
                                                <tr>
                                                     <td>SNo</td>
                                                    <td>Location Name</td>
                                                    <td>User Name</td>
                                                    <td>User Type</td>
                                                    <td>Edit</td>
                                                    <td>Delete</td>
                                                 </tr>
                                            </thead>
                                      </HeaderTemplate>
                                     
                                        <ItemTemplate>
                                        <tbody>
                                        
                                          <tr>
                                              <td>
                                                 <%# DataBinder.Eval(Container.DataItem, "SNo")%>
                                              </td>
                    
                                             <td>
                                                 <%# DataBinder.Eval(Container.DataItem, "LocCode")%>
                                             </td>
                                             <td>
                                                 <%# DataBinder.Eval(Container.DataItem, "UserName")%>
                                             </td>
                                             <td>
                                                  <%# DataBinder.Eval(Container.DataItem, "UserType")%>
                                            </td>
                                                                                    
                                            <td><p data-placement="top" data-toggle="tooltip" title="Edit"><asp:LinkButton ID="LinkButton1" runat="server" class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit"><span class="glyphicon glyphicon-pencil"></span></asp:LinkButton></p></td>
                                             
                                          
                                           
                                            <td><p data-placement="top" data-toggle="tooltip" title="Delete"><asp:LinkButton ID="LinkButton2" runat="server" class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete"><span class="glyphicon glyphicon-trash"></span></asp:LinkButton></p></td>
                                           
                                           </tr>
                                           </tbody>
                                     </ItemTemplate>
                                      <FooterTemplate>
                                        <tfoot>
                                                <tr>
                                                    <td>SNo</td>
                                                    <td>Location Name</td>
                                                    <td>User Name</td>
                                                    <td>User Type</td>
                                                    <td>Edit</td>
                                                    <td>Delete</td>
                                                </tr>
                                                  </table>
                                        </tfoot>
                                        </FooterTemplate>
                                      
                           </asp:Repeater>
                                   </div>
                                            
                                    
                                     </div>
                                    
                                            
                                        
                                        
                                           
                                            
                                           
                                           
                                           
                                            
                                         
                                  
                           
                           
                                    <%--  </div>
                            </div>
                            
                        </div>
                    </div><!-- Row -->
                </div>--%>
						  
						        
                  
                               <%-- <div class="panel-body">
                                   <div class="table-responsive">
                                   
                                     <asp:Repeater ID="Repeater1" runat="server">
                                         <HeaderTemplate>
                                   
                                        <table id="example" class="display table" style="width: 100%; cellspacing: 0;">
                                         <thead>
                                                <tr>
                                                    <th>Select</th>
                                                    <th>Location Name</th>
                                                    <th>User Name</th>
                                                    <th>User Type</th>
                                                </tr>
                                            </thead>
                                        </HeaderTemplate>
                                         <ItemTemplate>
                                           <tbody>
                                                 <tr>
                                                    <td><asp:CheckBox ID="CheckBox1" runat="server" /></td>
                                                    <td><%# Eval("LocCode") %></td>
                                                    <td><%# Eval("UserName") %></td>
                                                    <td><%# Eval("UserType") %></td>
                                                </tr>
                                            </tbody>
                                        </ItemTemplate>
                                         <FooterTemplate>
                                      </table>
                                 </FooterTemplate>
                                    </asp:Repeater>
                                         
                                     </div>
                               </div>--%>
                          
						
						
				</form>
			
			
			
                                                        
			
			
			
			
			
			
			
			
			</div><!-- panel white end -->
		    </div>
		    <div class="col-md-2"></div>
		    
            
      
 </div> <!-- col-9 end -->
        <!-- Dashboard start -->
 <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white" style="height: 100%;">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Dashboard Details</h4>
                                    <div class="panel-control">
                                        
                                        
                                    </div>
                                </div>
                                <div class="panel-body">
                                    
                                    
                                </div>
                            </div>
                        </div>  
                        
 <div class="col-lg-3 col-md-6">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile flip ha" data-mode="flip" data-speed="750" data-delay="3000">
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div class="live-tile carousel ha" data-mode="carousel" data-direction="horizontal" data-speed="750" data-delay="4500">
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                        </div>  
                        <!-- Dashboard End -->   
 </div><!-- col 12 end -->
      
  </div><!-- row end -->
 </div>
</div>
</asp:Content>

